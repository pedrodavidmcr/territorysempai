package com.senpaitools.territorysenpai.give.domain.usecase

import com.senpaitools.territorysenpai.common.domain.usecase.NotSuccessful

sealed class GiveError : NotSuccessful {
  object NoTerritoriesAvailable : GiveError()
}