package com.senpaitools.territorysenpai.give.domain.usecase

import com.senpaitools.territorysenpai.common.domain.model.FORMAT
import com.senpaitools.territorysenpai.common.domain.model.Status
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.repository.TerritoryRepository
import com.senpaitools.territorysenpai.common.domain.usecase.Either
import com.senpaitools.territorysenpai.common.domain.usecase.either
import com.senpaitools.territorysenpai.list.history.domain.model.Action
import com.senpaitools.territorysenpai.list.history.domain.model.History
import com.senpaitools.territorysenpai.list.history.domain.repository.HistoryRepository
import com.senpaitools.territorysenpai.list.history.domain.usecase.ActionUseCase
import khronos.Dates.today
import khronos.months
import khronos.plus
import khronos.toString

class GiveTerritory(
  private val repository: TerritoryRepository,
  history: HistoryRepository,
  private val territory: Territory,
  private val newOwner: String
) : ActionUseCase<Territory, GiveError>(
  history,
  History(today.toString(FORMAT), territory.number, newOwner, Action.GIVE, false)
) {
  override fun run(): Either<GiveError, Territory> =
    either {
      return@either repository.updateTerritory(
        territory.copy(
          status = Status.TAKEN,
          owner = newOwner,
          takenDate = today.toString(FORMAT),
          returnDate = (today + 4.months).toString(FORMAT),
          updated = false
        ),
        territory
      )
    }
}