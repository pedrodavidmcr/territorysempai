package com.senpaitools.territorysenpai.give.viewmodel

import androidx.lifecycle.MutableLiveData
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.executor.execute
import com.senpaitools.territorysenpai.common.inject.Injector
import com.senpaitools.territorysenpai.common.viewmodel.BackupViewModel

class GiveTerritoryViewModel : BackupViewModel() {
  val availableTerritories: MutableLiveData<List<Territory>> = MutableLiveData()

  private fun loadAvailableTerritories() =
    execute(
      Injector.getGetOldestTerritories(),
      availableTerritories, error
    )

  fun load() = loadAvailableTerritories()

  fun giveTerritory(territory: Territory, newOwner: String) {
    execute(
      Injector.getGiveTerritory(
        territory,
        newOwner
      ),
      updatedTerritory, error
    )
    backupTerritory.value = territory
  }


}