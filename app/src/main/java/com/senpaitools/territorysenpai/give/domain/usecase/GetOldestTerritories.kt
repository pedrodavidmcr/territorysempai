package com.senpaitools.territorysenpai.give.domain.usecase

import com.senpaitools.territorysenpai.common.domain.model.CONGREGATION
import com.senpaitools.territorysenpai.common.domain.model.FORMAT
import com.senpaitools.territorysenpai.common.domain.model.Status
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.repository.TerritoryRepository
import com.senpaitools.territorysenpai.common.domain.usecase.Either
import com.senpaitools.territorysenpai.common.domain.usecase.UseCase
import com.senpaitools.territorysenpai.common.domain.usecase.either
import khronos.toDate

class GetOldestTerritories(private val repository: TerritoryRepository) :
  UseCase<List<Territory>, GiveError> {
  override fun run(): Either<GiveError, List<Territory>> =
    either {
      val territoryList = repository
        .getAllTerritories(CONGREGATION)
        .filter { it.status == Status.AVAILABLE }
        .sortedBy { it.returnDate.toDate(FORMAT) }
      return@either if (territoryList.isEmpty()) GiveError.NoTerritoriesAvailable
      else territoryList
    }

}