package com.senpaitools.territorysenpai.give.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.senpaitools.territorysenpai.R
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.services.hideKeyboard
import com.senpaitools.territorysenpai.common.view.activity.getGridLayoutManagerByDeviceScreen
import com.senpaitools.territorysenpai.common.view.animation.SCREEN_POSITION
import com.senpaitools.territorysenpai.common.view.animation.TYPE
import com.senpaitools.territorysenpai.common.view.animation.generateAnimationValuesOf
import com.senpaitools.territorysenpai.common.view.extension.*
import com.senpaitools.territorysenpai.give.domain.usecase.GiveError
import com.senpaitools.territorysenpai.give.view.activity.GiveTerritoryActivity
import com.senpaitools.territorysenpai.give.view.adapter.SelectOneTerritoryAdapter
import com.senpaitools.territorysenpai.give.viewmodel.GiveTerritoryViewModel
import kotlinx.android.synthetic.main.fragment_give_territory.*

class GiveTerritoryFragment : Fragment() {
  private val viewModel: GiveTerritoryViewModel by lazy {
    ViewModelProviders.of(activity!!).get(GiveTerritoryViewModel::class.java)
  }
  private val adapter: SelectOneTerritoryAdapter by lazy {
    SelectOneTerritoryAdapter({ hasSelectedItem ->
      if (hasSelectedItem) giveButton.show()
      else giveButton.hide()
      view?.hideKeyboard()
    }, context!!)

  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.fragment_give_territory, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    viewModel.load()
    setUpFAB()
    setUpToolbar()
    setUpTerritoryList()
    setUpErrorHandling()
  }

  private fun showListIsEmpty() {
    loading.gone()
    container.addViewConstrainedToCenter(
      context!!.generateMessageTextView(getString(R.string.give_territory_list_empty))
    )
  }

  private fun setDataIntoList(list: List<Territory>) {
    view?.run {
      loading.visibility = View.GONE
      availableList.visibility = View.VISIBLE
      adapter.generateDataDividedByMonths(list)
    }
  }

  private fun setUpFAB() {
    giveButton.setOnClickListener { fab ->
      newOwner.text.toString().trim().let { newOwner ->
        if (newOwner.isEmpty()) showNewOwnerFieldCantBeEmptyError()
        else {
          adapter.currentTerritorySelected?.let { territory ->
            viewModel.giveTerritory(territory, newOwner)
            animateToConfirmationScreen(fab)
          }
        }
      }
    }
  }

  private fun animateToConfirmationScreen(viewToAnimate: View) {
    val animationValue = generateAnimationValuesOf(viewToAnimate)
    val extras = Bundle()
    extras.putSerializable(SCREEN_POSITION, animationValue)
    extras.putString(TYPE, "GIVE")
    (activity as GiveTerritoryActivity).showDetails(extras)
  }

  private fun showNewOwnerFieldCantBeEmptyError() {
    newOwner.error = getString(R.string.empty_input_text_error)
  }

  private fun setUpToolbar() {
    toolbar.navigationIcon = getDrawable(R.drawable.back)
    toolbar.setNavigationOnClickListener { activity!!.finish() }
  }

  private fun setUpTerritoryList() {
    availableList.setHideKeyboardWhenScroll()
    availableList.adapter = adapter
    availableList.layoutManager = context!!.getGridLayoutManagerByDeviceScreen()
      .apply {
        spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
          override fun getSpanSize(position: Int): Int =
            if (adapter.elementIsATerritory(position)) 1
            else spanCount
        }
      }
    viewModel.availableTerritories.observe(this, Observer {
      setDataIntoList(it)
    })
  }

  private fun setUpErrorHandling() {
    viewModel.error.observe(this, Observer {
      when (it) {
        GiveError.NoTerritoriesAvailable -> showListIsEmpty()
      }
    })
  }

}