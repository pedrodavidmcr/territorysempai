package com.senpaitools.territorysenpai.give.view.adapter

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.TransitionDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.senpaitools.territorysenpai.R
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.view.adapter.DataAdapter
import com.senpaitools.territorysenpai.common.view.extension.colorAccent
import com.senpaitools.territorysenpai.common.view.extension.colorPrimary
import kotlinx.android.synthetic.main.centered_text.view.*
import kotlinx.android.synthetic.main.items_available.view.*
import org.jetbrains.anko.backgroundColor


class SelectOneTerritoryAdapter(
  private val onExistAnItemSelected: (Boolean) -> Unit,
  private val context: Context
) : DataAdapter<SelectOneTerritoryAdapter.TypeElement, SelectOneTerritoryAdapter.Holder>() {

  private var currentTerritoryViewSelected: View? = null
  var currentTerritorySelected: Territory? = null
  private val isHEADER = 1
  private val isTERRITORY = 0

  override fun onBindViewHolder(holder: Holder, position: Int) = holder.setData(dataList[position])

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
    val layout = if (viewType == isTERRITORY) R.layout.items_available else R.layout.centered_text
    return Holder(
      LayoutInflater.from(parent.context)
        .inflate(layout, parent, false)
    )
  }

  override fun getItemViewType(position: Int): Int =
    if (dataList[position] is TypeElement.Header) isHEADER else isTERRITORY

  fun generateDataDividedByMonths(data: List<Territory>) {
    val territoryListWithMonthsStringSeparator =
      mutableListOf<TypeElement>().apply {
        addAll(getSectionOfTerritoriesIdleInTheRangeOfMonths(data, 300, 13))
        addAll(getSectionOfTerritoriesIdleInTheRangeOfMonths(data, 12, 10))
        addAll(getSectionOfTerritoriesIdleInTheRangeOfMonths(data, 9, 8))
        addAll(getSectionOfTerritoriesIdleInTheRangeOfMonths(data, 7, 6))
        addAll(getSectionOfTerritoriesIdleInTheRangeOfMonths(data, 5, 4))
        addAll(getSectionOfTerritoriesIdleInTheRangeOfMonths(data, 3, 2))
        addAll(getSectionOfTerritoriesIdleInTheRangeOfMonths(data, 1, 0))
      }
    super.updateData(territoryListWithMonthsStringSeparator)
  }

  private fun getSectionOfTerritoriesIdleInTheRangeOfMonths(
    data: List<Territory>,
    max: Int,
    min: Int
  ): Collection<SelectOneTerritoryAdapter.TypeElement> {
    val section = mutableListOf<TypeElement>()
    data.firstOrNull { it.monthsWithoutAssignation() in min..max }?.let {
      section.add(
        TypeElement.Header(
          context.getString(
            when {
              min >= 12 -> R.string.territory_idle_year
              min >= 2 -> R.string.territory_idle_months
              else -> R.string.territory_recently_worked
            }, min
          )
        )
      )

      section.addAll(data
        .filter { it.monthsWithoutAssignation() in min..max }
        .map {
          TypeElement.Item(it)
        }
      )
    }
    return section
  }

  fun elementIsATerritory(position: Int): Boolean = dataList[position] is TypeElement.Item

  inner class Holder(private val territoryView: View) :
    RecyclerView.ViewHolder(territoryView) {
    fun setData(sectionedElement: TypeElement) = when (sectionedElement) {
      is TypeElement.Header -> sectionedElement.value.run {
        territoryView.monthsIdle.text = this
      }
      is TypeElement.Item -> sectionedElement.value.run {
        territoryView.territoryNumber.text = number.toString()
        highlightTerritoryViewIfIsSelected(territoryView, number)

        territoryView.setOnClickListener { selectedTerritoryView ->
          animateDeselection(currentTerritoryViewSelected)

          if (currentTerritorySelected != this) {
            animateSelection(selectedTerritoryView)
            currentTerritorySelected = this
            currentTerritoryViewSelected = selectedTerritoryView
          } else {
            currentTerritoryViewSelected = null
            currentTerritorySelected = null
          }

          onExistAnItemSelected(currentTerritoryViewSelected != null)
        }
      }
    }

    private fun highlightTerritoryViewIfIsSelected(territoryView: View, territoryNumber: Int) {
      territoryView.backgroundColor = if (territoryNumber == currentTerritorySelected?.number)
        territoryView.context.colorAccent()
      else territoryView.context.colorPrimary()
    }

    private fun animateDeselection(territoryView: View?) = territoryView?.let {
      animateColorChange(it, it.context.colorAccent(), it.context.colorPrimary())
    }

    private fun animateSelection(territoryView: View?) = territoryView?.let {
      animateColorChange(it, it.context.colorPrimary(), it.context.colorAccent())
    }

    private fun animateColorChange(it: View, firstColor: Int, secondColor: Int) {
      val colorTransition = arrayOf(ColorDrawable(firstColor), ColorDrawable(secondColor))
      val animation = TransitionDrawable(colorTransition)
      it.background = animation
      animation.startTransition(190)
    }
  }

  sealed class TypeElement {
    class Header(val value: String) : TypeElement()
    class Item(val value: Territory) : TypeElement()
  }
}