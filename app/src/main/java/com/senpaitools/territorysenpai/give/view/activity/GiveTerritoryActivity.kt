package com.senpaitools.territorysenpai.give.view.activity

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.senpaitools.territorysenpai.R
import com.senpaitools.territorysenpai.common.view.activity.BackupActivity
import com.senpaitools.territorysenpai.give.view.fragment.GiveTerritoryFragment
import kotlinx.android.synthetic.main.activity_give_territory.*


class GiveTerritoryActivity : BackupActivity() {
  private val listFragment: GiveTerritoryFragment = GiveTerritoryFragment()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_give_territory)
    updateFragment(listFragment)
  }

  private fun updateFragment(fragment: Fragment) = super.updateFragment(R.id.container, fragment)

  fun showDetails(bundle: Bundle) = super.showDetails(R.id.container, bundle)

  fun quitList() = super.quitList(container, listFragment)
}
