package com.senpaitools.territorysenpai.list.history.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.senpaitools.territorysenpai.R
import com.senpaitools.territorysenpai.common.view.adapter.DataAdapter
import com.senpaitools.territorysenpai.common.view.extension.gone
import com.senpaitools.territorysenpai.common.view.extension.visible
import com.senpaitools.territorysenpai.list.history.domain.model.Action
import com.senpaitools.territorysenpai.list.history.domain.model.History
import kotlinx.android.synthetic.main.items_history.view.*
import org.jetbrains.anko.imageResource

class HistoryAdapter : DataAdapter<History, HistoryAdapter.Holder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder =
    Holder(
      LayoutInflater.from(parent.context)
        .inflate(R.layout.items_history, parent, false)
    )

  override fun onBindViewHolder(holder: Holder, position: Int) =
    holder.setData(dataList[position])

  inner class Holder(private val view: View) : RecyclerView.ViewHolder(view) {
    fun setData(history: History) = with(view) {
      takenDateText.text = history.date
      description.text = descriptionOf(history)
      historyIcon.imageResource = imageOf(history)
      if (!history.updated) {
        notUpdatedIcon.visible()
      } else notUpdatedIcon.gone()
    }


    private fun imageOf(history: History): Int =
      when (history.action) {
        Action.GIVE -> R.drawable.give
        Action.RETURN -> R.drawable.refund
        Action.EDIT -> R.drawable.edit
        else -> R.drawable.inverse_check
      }

    private fun descriptionOf(history: History): String =
      when (history.action) {
        Action.GIVE -> view.context.getString(
          R.string.history_give_territory,
          history.number,
          history.owner
        )
        Action.RETURN -> view.context.getString(
          R.string.history_return_territory,
          history.owner,
          history.number
        )
        Action.ADD -> view.context.getString(
          R.string.history_give_territory,
          history.number,
          history.owner
        )
        Action.DELETE -> ""
        Action.EDIT -> view.context.getString(R.string.history_edit_territory, history.number)
      }
  }
}