package com.senpaitools.territorysenpai.list.history.domain.usecase

import com.senpaitools.territorysenpai.common.domain.usecase.UseCase
import com.senpaitools.territorysenpai.list.history.domain.model.History
import com.senpaitools.territorysenpai.list.history.domain.repository.HistoryRepository
import org.jetbrains.anko.doAsync

abstract class ActionUseCase<V, E>(
  repository: HistoryRepository,
  history: History
) : UseCase<V, E> {
  init {
    doAsync {
      val identifiedHistory = history.copy(id = repository.getHistoryCount())
      repository.addHistoryRegister(identifiedHistory)
    }
  }
}