package com.senpaitools.territorysenpai.list.territories.usecase

import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.repository.TerritoryRepository
import com.senpaitools.territorysenpai.common.domain.usecase.Either
import com.senpaitools.territorysenpai.common.domain.usecase.UseCase
import com.senpaitools.territorysenpai.common.domain.usecase.either

class GetNotUpdatedTerritories(val repository: TerritoryRepository) :
  UseCase<List<Territory>, GetError> {
  override fun run(): Either<GetError, List<Territory>> =
    either {
      val notUpdatedTerritories = repository.getAllTerritories("cabanyal")
        .filter { !it.updated }
        .sortedBy { it.number }
      return@either if (notUpdatedTerritories.isEmpty()) GetError.NoUnregisteredTerritories
      else notUpdatedTerritories
    }
}