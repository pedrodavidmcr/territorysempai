package com.senpaitools.territorysenpai.list.edition.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.senpaitools.territorysenpai.R
import com.senpaitools.territorysenpai.common.domain.model.Status
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.view.animation.ProgressAnimation
import com.senpaitools.territorysenpai.common.view.animation.setOnEndListener
import com.senpaitools.territorysenpai.common.view.animation.startColorAnimation
import com.senpaitools.territorysenpai.common.view.extension.*
import com.senpaitools.territorysenpai.list.general.view.activity.MainList
import com.senpaitools.territorysenpai.list.general.viewmodel.MainListViewModel
import com.senpaitools.territorysenpai.list.history.domain.model.History
import com.senpaitools.territorysenpai.list.history.view.adapter.HistoryAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_details.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.textColor

class DetailsFragment : Fragment() {
  private val viewModel: MainListViewModel by lazy {
    ViewModelProviders.of(activity!!).get(MainListViewModel::class.java)
  }
  private val adapter = HistoryAdapter()
  lateinit var editTerritoryHandler: (Territory) -> Unit

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.fragment_details, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    initHistoryList()
    viewModel.selectedTerritory.observe(this, Observer { territory ->
      territory?.let {
        configureFragmentWithTerritoryData(it)
        initFABs(it)
      }
    })
  }

  private fun initHistoryList() {
    detailsHistory.layoutManager = LinearLayoutManager(context)
    detailsHistory.adapter = adapter
    viewModel.changesOfTerritory.observe(this, Observer { changes ->
      changes?.let {
        if (it.isEmpty()) showTerritoryHasNoUnregisteredChanges()
        else showUnregisteredChangesOfTerritory(it)
      }
    })
  }

  private fun initFABs(territory: Territory) {
    editTerritory.setOnClickListener { editTerritoryHandler(territory) }
    clearHistory.setOnClickListener { clearHistoryListWithAnimation(territory) }
  }

  private fun clearHistoryListWithAnimation(territory: Territory) {
    viewModel.putChangesAsRegistered(territory)
    showTerritoryHasNoUnregisteredChanges()
  }

  private fun showUnregisteredChangesOfTerritory(list: List<History>) {
    loadingHistory?.isIndeterminate = false
    detailsHistory?.visible()
    loadingHistory?.startAnimation(
      ProgressAnimation(loadingHistory, 0F, 100F)
        .apply {
        duration = 1000
        setOnEndListener {
          clearHistory?.show()
          adapter.updateData(list)
          loadingHistory?.gone()
        }
      })
  }

  fun fadeIn() {
    val anim = AnimationUtils.loadAnimation(
      activity?.applicationContext,
      R.anim.fade_in
    )
    territoryContainer.startAnimation(anim)
  }

  private fun configureFragmentWithTerritoryData(territory: Territory) {
    setTerritoryCardData(territory)
    setTerritoryCardColor(selectColor(territory))
    adapter.clear()
    stateHistoryText.gone()
    if (territory.updated) {
      showTerritoryHasNoUnregisteredChanges()
    } else {
      loadingHistory.visible()
      viewModel.loadChangesFromTerritory(territory)
    }
  }

  private fun setTerritoryCardData(territory: Territory) {
    territory.apply {
      activity?.let {
        (it as MainList).toolbar.title = getString(R.string.territory_with_number, number)
      }
      numberText.text = number.toString()
      zoneText.text = zone
      ownerText.text = owner
      takenDateText.text = takenDate
      returnDateText.text = returnDate
      numberText.textColor = if (updated) context!!.colorWhite()
      else context!!.colorRed()
      returnDateTitle.text = if (status == Status.TAKEN) context!!.getString(R.string.expiry_date)
      else context!!.getString(R.string.last_return)
      takenDateTitle.text = if (status == Status.TAKEN) context!!.getString(R.string.delivery_date)
      else context!!.getString(R.string.last_delivery)
    }
  }

  private fun showTerritoryHasNoUnregisteredChanges() {
    clearHistory.hide()
    adapter.clear()
    loadingHistory.gone()
    startColorAnimation(historyContainer,
      context!!.colorWhite(),
      context!!.colorPrimary70(),
      400,
      whenFinished = {
        stateHistoryText.visible()
        detailsHistory.gone()
        numberText.textColor = context!!.colorWhite()
      })
  }

  private fun setTerritoryCardColor(color: Int) {
    view?.run {
      numberText.backgroundColor = color
      zoneText.textColor = color
      takenDateTitle.backgroundColor = color
      returnDateTitle.backgroundColor = color
    }
  }

  private fun selectColor(territory: Territory): Int = when {
    territory.isTimedOut() -> context!!.colorTimedOut()
    territory.status == Status.TAKEN -> context!!.colorTaken()
    else -> context!!.colorPrimary()
  }
}