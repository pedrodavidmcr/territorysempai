package com.senpaitools.territorysenpai.list.territories.usecase

import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.repository.TerritoryRepository
import com.senpaitools.territorysenpai.common.domain.usecase.Either
import com.senpaitools.territorysenpai.common.domain.usecase.UseCase
import com.senpaitools.territorysenpai.common.domain.usecase.either

class GetAllTerritories(
  private val repository: TerritoryRepository,
  private val congregation: String
) : UseCase<List<Territory>, GetError> {
  override fun run(): Either<GetError, List<Territory>> =
    either {
      val territories = repository.getAllTerritories(congregation).sortedBy { it.number }
      return@either if (territories.isEmpty()) GetError.NoTerritories
      else territories
    }
}