package com.senpaitools.territorysenpai.list.general.view.custom

import com.mikepenz.crossfader.Crossfader
import com.mikepenz.materialdrawer.interfaces.ICrossfader


class CrossFadeWrapper(private val crossFade: Crossfader<*>?) : ICrossfader {
  override fun isCrossfaded(): Boolean = crossFade!!.isCrossFaded()

  override fun crossfade() {
    crossFade!!.crossFade()
  }

}