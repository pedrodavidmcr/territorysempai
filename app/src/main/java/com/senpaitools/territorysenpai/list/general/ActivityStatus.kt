package com.senpaitools.territorysenpai.list.general

sealed class ActivityStatus {
  object Loading : ActivityStatus()
  object Reloading : ActivityStatus()
  object Loaded : ActivityStatus()
}