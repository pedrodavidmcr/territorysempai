package com.senpaitools.territorysenpai.list.territories.usecase

sealed class GetError {
  object NoTerritories : GetError()
  object NoOutDatedTerritories : GetError()
  object NoUnregisteredTerritories : GetError()
  object NoHistory : GetError()
  object FirebaseError : GetError()
}