package com.senpaitools.territorysenpai.list.edition.view.fragment

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import com.senpaitools.territorysenpai.common.domain.model.FORMAT
import khronos.toDate
import java.util.*

class TimePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

  override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
    val c = Calendar.getInstance().apply { set(year, month, dayOfMonth) }
    targetFragment?.let {
      it.onActivityResult(targetRequestCode,
        Activity.RESULT_OK,
        Intent().apply { putExtra(DATE, c.time) })
    }
  }


  override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
    val date = arguments?.getString(DATE)?.toDate(FORMAT)
    val c = Calendar.getInstance()
    date?.let { c.time = it }
    val year = c.get(Calendar.YEAR)
    val month = c.get(Calendar.MONTH)
    val day = c.get(Calendar.DAY_OF_MONTH)

    return DatePickerDialog(context!!, this, year, month, day)
  }
}

const val DATE = "date"



