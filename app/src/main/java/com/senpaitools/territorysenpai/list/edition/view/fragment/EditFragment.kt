package com.senpaitools.territorysenpai.list.edition.view.fragment

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.senpaitools.territorysenpai.R
import com.senpaitools.territorysenpai.common.domain.model.FORMAT
import com.senpaitools.territorysenpai.common.domain.model.Status
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.view.animation.RevealAnimationSettings
import com.senpaitools.territorysenpai.common.view.extension.colorPrimary
import com.senpaitools.territorysenpai.common.view.extension.colorTaken
import com.senpaitools.territorysenpai.common.view.extension.colorTimedOut
import com.senpaitools.territorysenpai.list.general.viewmodel.MainListViewModel
import khronos.toString
import kotlinx.android.synthetic.main.edit_fragment.*
import org.jetbrains.anko.backgroundColor
import java.util.*

class EditFragment : Fragment() {
  private val viewModel: MainListViewModel by lazy {
    ViewModelProviders.of(activity!!).get(MainListViewModel::class.java)
  }
  private val REQUEST_TAKEN_DATE = 0
  private val REQUEST_RETURN_DATE = 1
  private lateinit var animationSettings: RevealAnimationSettings

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.edit_fragment, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    viewModel.selectedTerritory.observe(this, Observer { territory ->
      territory?.apply {
        initFAB(this)
        setData(this)
      }
    })
    setPickers()
    animationSettings = arguments!!.getSerializable("animation") as RevealAnimationSettings
  }

  private fun initFAB(territory: Territory) {
    fadeInAnim()
    editFragmentUpdateFab.setOnClickListener {
      viewModel.editTerritory(getEditedTerritory(territory.updated), territory)
      activity?.onBackPressed()
    }
  }

  private fun setSwitch() {
    setSwitchText()
    editFragmentStatusButton.setOnCheckedChangeListener { _, _ ->
      setSwitchText()
    }
  }

  private fun setSwitchText() {
    with(edit_fragment_textSwitcher) {
      when (editFragmentStatusButton.isChecked) {
        true -> {
          slideRightAnimation()
          setText(getString(R.string.available_text))
        }
        else -> {
          slideLeftAnimation()
          setText(getString(R.string.status_button_takenText))
        }
      }
    }
  }

  private fun slideLeftAnimation() {
    with(edit_fragment_textSwitcher) {
      inAnimation = AnimationUtils.loadAnimation(
        activity,
        R.anim.slide_in_right
      )
      outAnimation = AnimationUtils.loadAnimation(
        activity,
        R.anim.slide_out_left
      )
    }
  }

  private fun slideRightAnimation() {
    with(edit_fragment_textSwitcher) {
      inAnimation = AnimationUtils.loadAnimation(
        activity,
        android.R.anim.slide_in_left
      )
      outAnimation = AnimationUtils.loadAnimation(
        activity,
        android.R.anim.slide_out_right
      )

    }
  }

  private fun setPickers() {
    with(TimePickerFragment()) {
      this@EditFragment.editFragmentTakenDateInput.setOnClickListener {
        arguments = android.os.Bundle().apply {
          putString(DATE, this@EditFragment.editFragmentTakenDateInput.text.toString())
        }
        setTargetFragment(this@EditFragment, REQUEST_TAKEN_DATE)
        show(this@EditFragment.activity?.supportFragmentManager, "datePicker")
      }

      this@EditFragment.editFragmentReturnDateInput.setOnClickListener {
        arguments = android.os.Bundle().apply {
          putString(DATE, this@EditFragment.editFragmentReturnDateInput.text.toString())
        }
        setTargetFragment(this@EditFragment, REQUEST_RETURN_DATE)
        show(this@EditFragment.activity?.supportFragmentManager, "datePicker")
      }
    }
  }


  private fun getEditedTerritory(updated: Boolean) = Territory(
    editFragmentNumberInput.text.toString().toInt(),
    editFragmentZoneInput.text.toString(),
    if (editFragmentStatusButton.isChecked) Status.AVAILABLE else Status.TAKEN,
    editFragmentTakenDateInput.text.toString(),
    editFragmentReturnDateInput.text.toString(),
    editFragmentOwnerInput.text.toString(),
    updated
  )


  private fun setData(territory: Territory) = territory.apply {
    editFragmentNumberInput.setText(number.toString())
    editFragmentZoneInput.setText(zone)
    editFragmentOwnerInput.setText(owner)
    editFragmentTakenDateInput.text = takenDate
    editFragmentReturnDateInput.text = returnDate
    editFragmentStatusButton.isChecked = status == Status.AVAILABLE
    setTerritoryCardColor(this)
    setSwitch()
  }

  private fun fadeInAnim() {
    editFragmentCardViewConstraint.startAnimation(
      AnimationUtils.loadAnimation(
        activity?.applicationContext, R.anim.fade_in
      )
    )
    Handler().postDelayed({ editFragmentUpdateFab?.show() }, 300)
  }

  fun fadeOutAnim() {
    editFragmentUpdateFab.hide()
    editFragmentCardViewConstraint.startAnimation(
      AnimationUtils.loadAnimation(
        activity?.applicationContext, R.anim.fade_out
      )
    )
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    if (requestCode == REQUEST_TAKEN_DATE) {
      val date = data?.getSerializableExtra(DATE) as Date
      editFragmentTakenDateInput.text = date.toString(FORMAT)
    }
    if (requestCode == REQUEST_RETURN_DATE) {
      val date = data?.getSerializableExtra(DATE) as Date
      editFragmentReturnDateInput.text = date.toString(FORMAT)
    }
  }


  private fun setTerritoryCardColor(territory: Territory) {
    val color = when {
      territory.isTimedOut() -> context!!.colorTimedOut()
      territory.status == Status.TAKEN -> context!!.colorTaken()
      else -> context!!.colorPrimary()
    }
    separator3.backgroundColor = color
    edit_fragment_separator1.backgroundColor = color
    editFragmentTakenDateTitle.backgroundColor = color
    editFragmentReturnDateTitle.backgroundColor = color
  }
}