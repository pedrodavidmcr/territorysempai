package com.senpaitools.territorysenpai.list.territories.usecase

import com.senpaitools.territorysenpai.common.domain.model.FORMAT
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.repository.TerritoryRepository
import com.senpaitools.territorysenpai.common.domain.usecase.Either
import com.senpaitools.territorysenpai.common.domain.usecase.UseCase
import com.senpaitools.territorysenpai.common.domain.usecase.either
import khronos.toDate

class GetTimedOutTerritories(
  private val repository: TerritoryRepository
) : UseCase<List<Territory>, GetError> {
  override fun run(): Either<GetError, List<Territory>> =
    either {
      val timedOutTerritories = repository.getAllTerritories("cabanyal")
        .filter { it.isTimedOut() }
        .sortedBy { it.takenDate.toDate(FORMAT) }
      return@either if (timedOutTerritories.isEmpty()) GetError.NoOutDatedTerritories
      else timedOutTerritories
    }
}