package com.senpaitools.territorysenpai.list.history.domain.usecase

import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.repository.TerritoryRepository
import com.senpaitools.territorysenpai.common.domain.usecase.Either
import com.senpaitools.territorysenpai.common.domain.usecase.UseCase
import com.senpaitools.territorysenpai.common.domain.usecase.either
import com.senpaitools.territorysenpai.list.history.domain.model.History
import com.senpaitools.territorysenpai.list.history.domain.repository.HistoryRepository
import com.senpaitools.territorysenpai.list.territories.usecase.GetError

class RegisterChangesFromTerritory(
  private val repository: HistoryRepository,
  private val territoryRepository: TerritoryRepository,
  private val territory: Territory
) : UseCase<List<History>, GetError> {
  override fun run(): Either<GetError, List<History>> =
    either {
      repository.clearTerritoryHistory(territory)
      territoryRepository.updateTerritory(territory.copy(updated = true), territory)
      return@either emptyList<History>()
    }

}