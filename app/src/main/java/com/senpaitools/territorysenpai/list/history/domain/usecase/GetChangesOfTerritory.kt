package com.senpaitools.territorysenpai.list.history.domain.usecase

import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.usecase.Either
import com.senpaitools.territorysenpai.common.domain.usecase.UseCase
import com.senpaitools.territorysenpai.common.domain.usecase.either
import com.senpaitools.territorysenpai.list.history.domain.model.History
import com.senpaitools.territorysenpai.list.history.domain.repository.HistoryRepository
import com.senpaitools.territorysenpai.list.territories.usecase.GetError

class GetChangesOfTerritory(
  private val repository: HistoryRepository,
  private val territory: Territory
) : UseCase<List<History>, GetError> {
  override fun run(): Either<GetError, List<History>> =
    either {
      return@either repository.getHistory()
        .filter { !it.updated && it.number == territory.number }
    }
}