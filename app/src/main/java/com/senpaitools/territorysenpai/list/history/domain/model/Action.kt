package com.senpaitools.territorysenpai.list.history.domain.model

enum class Action {
  GIVE, RETURN, ADD, DELETE, EDIT
}