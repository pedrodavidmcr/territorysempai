package com.senpaitools.territorysenpai.list.territories.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.senpaitools.territorysenpai.R
import com.senpaitools.territorysenpai.common.view.activity.getGridLayoutManagerByDeviceScreen
import com.senpaitools.territorysenpai.common.view.extension.*
import com.senpaitools.territorysenpai.list.general.ActivityStatus
import com.senpaitools.territorysenpai.list.general.viewmodel.MainListViewModel
import com.senpaitools.territorysenpai.list.history.view.adapter.HistoryAdapter
import com.senpaitools.territorysenpai.list.territories.usecase.GetError
import com.senpaitools.territorysenpai.list.territories.view.adapter.SimpleTerritoryListAdapter
import kotlinx.android.synthetic.main.fragment_list_with_loading.*

class ListTerritoriesFragment : Fragment() {
  private val viewModel: MainListViewModel by lazy {
    ViewModelProviders.of(activity!!).get(MainListViewModel::class.java)
  }
  private val territoryAdapter: SimpleTerritoryListAdapter = SimpleTerritoryListAdapter {
    viewModel.selectedTerritory.value = it
  }
  private val historyAdapter: HistoryAdapter = HistoryAdapter()

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.fragment_list_with_loading, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    configureListToTerritories()
    handleActivityStatusChanges()
    handleDataOnList()
    handleErrors()
  }

  private fun handleActivityStatusChanges() {
    viewModel.activityStatus.observe(this, Observer {
      when (it) {
        ActivityStatus.Loading -> {
          container.removeEmptyListTextView()
          globalList.gone()
        }
      }
    })
  }

  private fun handleErrors() {
    viewModel.error.observe(this, Observer { error ->
      viewModel.activityStatus.value = ActivityStatus.Loaded
      error?.let {
        container.addViewConstrainedToCenter(
          context!!.generateMessageTextView(
            when (it) {
              GetError.NoTerritories -> context!!.getString(R.string.territory_list_empty)
              GetError.NoOutDatedTerritories -> context!!.getString(R.string.outdated_list_empty)
              GetError.NoUnregisteredTerritories -> context!!.getString(R.string.not_registered_list_empty)
              GetError.NoHistory -> context!!.getString(R.string.history_empty)
              GetError.FirebaseError -> TODO()
            }
          )
        )
      }
    })
  }

  private fun handleDataOnList() {
    viewModel.history.observe(this, Observer { historyList ->
      historyList?.let {
        viewModel.activityStatus.value = ActivityStatus.Loaded
        configureListToHistory()
        historyAdapter.updateData(it)
      }
    })
    viewModel.territoryList.observe(this, Observer { territoryList ->
      territoryList?.let {
        viewModel.activityStatus.value = ActivityStatus.Loaded
        configureListToTerritories()
        territoryAdapter.updateData(it)
      }
    })
  }

  private fun configureListToTerritories() {
    container.removeEmptyListTextView()
    globalList.visible()
    globalList.adapter = territoryAdapter
    globalList.layoutManager = context!!.getGridLayoutManagerByDeviceScreen()
  }

  private fun configureListToHistory() {
    container.removeEmptyListTextView()
    globalList.visible()
    globalList.adapter = historyAdapter
    globalList.layoutManager = LinearLayoutManager(context)
  }

}
