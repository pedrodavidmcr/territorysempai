package com.senpaitools.territorysenpai.list.general.view.activity

import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import android.transition.TransitionSet
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.mikepenz.crossfader.Crossfader
import com.mikepenz.crossfader.util.UIUtils
import com.mikepenz.crossfader.view.CrossFadeSlidingPaneLayout
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.MiniDrawer
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.SectionDrawerItem
import com.senpaitools.territorysenpai.R
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.view.activity.isTablet
import com.senpaitools.territorysenpai.common.view.animation.MENU_POSITION
import com.senpaitools.territorysenpai.common.view.animation.RevealAnimationSettings
import com.senpaitools.territorysenpai.common.view.animation.coordinatesForCenterOf
import com.senpaitools.territorysenpai.common.view.extension.gone
import com.senpaitools.territorysenpai.common.view.extension.isPreLollipop
import com.senpaitools.territorysenpai.common.view.extension.isVisible
import com.senpaitools.territorysenpai.common.view.extension.visible
import com.senpaitools.territorysenpai.give.view.activity.GiveTerritoryActivity
import com.senpaitools.territorysenpai.list.edition.view.fragment.DetailsFragment
import com.senpaitools.territorysenpai.list.edition.view.fragment.EditFragment
import com.senpaitools.territorysenpai.list.general.ActivityStatus
import com.senpaitools.territorysenpai.list.general.view.custom.CrossFadeWrapper
import com.senpaitools.territorysenpai.list.general.viewmodel.MainListViewModel
import com.senpaitools.territorysenpai.list.territories.view.fragment.ListTerritoriesFragment
import com.senpaitools.territorysenpai.refund.view.activity.ReturnTerritoryActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_details.*
import org.jetbrains.anko.startActivity

class MainList : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {
  private val viewModel: MainListViewModel by lazy {
    ViewModelProviders.of(this).get(MainListViewModel::class.java)
  }
  private lateinit var drawer: Drawer
  private val tabAll: PrimaryDrawerItem by lazy {
    PrimaryDrawerItem()
      .withName(getString(R.string.all))
      .withIcon(R.drawable.all)
      .withIdentifier(1)
  }
  private val tabTimedOut: PrimaryDrawerItem by lazy {
    PrimaryDrawerItem()
      .withName(getString(R.string.timedOut))
      .withIcon(R.drawable.outdated)
      .withIdentifier(2)
  }
  private val tabNotUpdated: PrimaryDrawerItem by lazy {
    PrimaryDrawerItem()
      .withName(getString(R.string.notUpdated))
      .withIcon(R.drawable.not_updated)
      .withIdentifier(3)
  }
  private val tabHistory: PrimaryDrawerItem by lazy {
    PrimaryDrawerItem()
      .withName(getString(R.string.history))
      .withIcon(R.drawable.history)
      .withIdentifier(4)
  }
  private val detailsFragment = DetailsFragment()
  private val listFragment = ListTerritoriesFragment()
  private val editFragment = EditFragment()
  private val onClickToEditTerritory: (Territory) -> Unit = {
    setSharedElementsAnim()
    addFragmentWithAnimation(editFragment)
    if (!isTablet()) hideFABs()
    editFragment.arguments = Bundle().apply {
      putSerializable(
        "animation", RevealAnimationSettings(
          (detailsFragment.territoryContainer.x + detailsFragment.territoryContainer.width / 2).toInt(),
          (detailsFragment.territoryContainer.y + detailsFragment.territoryContainer.height / 2).toInt(),
          detailsFragment.territoryContainer.width,
          detailsFragment.territoryContainer.height
        )
      )
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    setSupportActionBar(toolbar)
    initViews()
    detailsFragment.editTerritoryHandler = onClickToEditTerritory
    handleActivityStatusChanges()
    handleTerritorySelection()
    setFragment(listFragment)
    viewModel.activityStatus.value = ActivityStatus.Loading
    selectTabList(tabAll.identifier.toInt())
  }

  override fun onRefresh() {
    viewModel.activityStatus.value = ActivityStatus.Reloading
    if (isTablet()) selectTabList(drawer.currentSelection.toInt())
    else {
      when (currentActiveFragment()) {
        is EditFragment -> viewModel.activityStatus.value = ActivityStatus.Loaded
        is DetailsFragment -> viewModel.activityStatus.value = ActivityStatus.Loaded
        else -> selectTabList(drawer.currentSelection.toInt())
      }
    }
  }

  override fun onBackPressed() {
    when (currentActiveFragment()) {
      is EditFragment -> {
        editFragmentCompleteEdition()
      }
      is DetailsFragment -> {
        viewModel.changesOfTerritory.value = null
        if (isTablet()) tabletCloseSecondFragment()
        else {
          setFragment(listFragment)
          viewModel.activityStatus.value = ActivityStatus.Loading
          selectTabList(drawer.currentSelection.toInt())
          refresh.isEnabled = true
          enableDrawer()
        }
      }
      else -> finish()
    }
  }

  override fun onResume() {
    selectTabList(drawer.currentSelection.toInt())
    super.onResume()
  }

  private fun setSharedElementsAnim() {
    if (!isPreLollipop()) {
      with(TransitionSet()) {
        addTransition(
          TransitionInflater.from(this@MainList)
            .inflateTransition(android.R.transition.move)
        )
        duration = 300
        interpolator = FastOutSlowInInterpolator()
        editFragment.sharedElementEnterTransition = this
      }
    }
  }

  private fun initViews() {
    refresh.setOnRefreshListener(this)
    if (isTablet()) setUpMiniDrawer()
    else {
      requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
      setUpDrawer()
    }
    setUpFABs()
  }

  private fun setFragment(fragment: Fragment) {
    supportFragmentManager
      .beginTransaction().replace(R.id.container, fragment).commitAllowingStateLoss()
  }

  @Suppress("PLUGIN_WARNING")
  private fun setFragmentOnSplitScreen(fragment: Fragment) {
    supportFragmentManager
      .beginTransaction().replace(R.id.secondary_container, fragment).commitAllowingStateLoss()
    secondary_container.visible()
  }

  private fun addFragment(fragment: Fragment) {
    val container = if (isTablet()) R.id.secondary_container else R.id.container
    supportFragmentManager
      .beginTransaction().replace(container, fragment)
      .addToBackStack(fragment.tag)
      .commitAllowingStateLoss()
  }

  private fun addFragmentWithAnimation(fragment: Fragment) {
    val container = if (isTablet()) R.id.secondary_container else R.id.container
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      supportFragmentManager
        .beginTransaction()
        .addSharedElement(takenDateTitle, takenDateTitle.transitionName)
        .addSharedElement(returnDateTitle, returnDateTitle.transitionName)
        .addSharedElement(territoryContainer, territoryContainer.transitionName)
        .replace(container, fragment)
        .addToBackStack(fragment.tag)
        .commit()
    } else addFragment(fragment)
  }

  private fun setUpFABs() {
    returnButton.setOnClickListener {
      val (centerX, centerY) = coordinatesForCenterOf(it)
      startActivity<ReturnTerritoryActivity>(
        MENU_POSITION to
            RevealAnimationSettings(centerX, centerY, container.width, container.height)
      )
    }
    giveButton.setOnClickListener {
      val (centerX, centerY) = coordinatesForCenterOf(it)
      startActivity<GiveTerritoryActivity>(
        MENU_POSITION to
            RevealAnimationSettings(centerX, centerY, container.width, container.height)
      )
    }
  }

  private fun setUpDrawer() {
    drawer = DrawerBuilder().withActivity(this)
      .addDrawerItems(
        SectionDrawerItem().withName("Territorios"),
        tabAll,
        tabTimedOut,
        tabNotUpdated,
        SectionDrawerItem().withName("Registros"),
        tabHistory
      )
      .withTranslucentStatusBar(true)
      .withActionBarDrawerToggle(true)
      .withToolbar(toolbar)
      .withSelectedItem(tabAll.identifier)
      .build()
    addChangeTabListener(drawer)
  }

  private fun setUpMiniDrawer() {
    drawer = generateNavigationDrawer()
    addChangeTabListener(drawer)
    val mini = drawer.miniDrawer
    val crossFade = generateCrossFadingAnimation(drawer, mini)
    mini.withCrossFader(CrossFadeWrapper(crossFade))
    crossFade.getCrossFadeSlidingPaneLayout()
      .setShadowResourceLeft(R.drawable.material_drawer_shadow_left)
  }

  private fun handleTerritorySelection() {
    viewModel.selectedTerritory.observe(this, Observer { territory ->
      territory?.let {
        if (isTablet()) setFragmentOnSplitScreen(detailsFragment)
        else {
          setFragment(detailsFragment)
          hideFABs()
          refresh.isEnabled = false
          disableDrawer()
        }
      }
    })
  }

  private fun disableDrawer() {
    drawer.actionBarDrawerToggle.isDrawerIndicatorEnabled = false
    drawer.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
  }

  private fun enableDrawer() {
    drawer.actionBarDrawerToggle.isDrawerIndicatorEnabled = true
    drawer.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
  }

  private fun handleActivityStatusChanges() {
    viewModel.activityStatus.observe(this, Observer { status ->
      when (status) {
        ActivityStatus.Loading -> if (!refresh.isRefreshing) loading.visible()
        ActivityStatus.Loaded -> {
          loading.gone()
          refresh.isRefreshing = false
        }
      }
    })
  }

  private fun generateCrossFadingAnimation(drawer: Drawer, mini: MiniDrawer): Crossfader<*> {
    val firstWidth = UIUtils.convertDpToPixel(300f, this).toInt()
    val secondWidth = UIUtils.convertDpToPixel(72f, this).toInt()
    return Crossfader<CrossFadeSlidingPaneLayout>()
      .withContent(tablet_dual_container)
      .withFirst(drawer.slider, firstWidth)
      .withSecond(mini.build(this), secondWidth)
      .build()
  }

  private fun generateNavigationDrawer(): Drawer {
    return DrawerBuilder().withActivity(this)
      .withToolbar(toolbar)
      .withHeader(R.layout.header)
      .withGenerateMiniDrawer(true)
      .addDrawerItems(
        SectionDrawerItem().withName("Territorios"),
        tabAll,
        tabTimedOut,
        tabNotUpdated,
        SectionDrawerItem().withName("Registros"),
        tabHistory
      )
      .withSelectedItem(tabAll.identifier)
      .buildView()
  }

  private fun selectTabList(tab: Int): Boolean {
    selectTabTitle()
    when (tab.toLong()) {
      tabHistory.identifier -> {
        hideFABs()
        viewModel.loadHistory()
      }
      else -> {
        showFABs()
        when (tab.toLong()) {
          tabAll.identifier -> viewModel.loadAll()
          tabTimedOut.identifier -> viewModel.loadTimedOut()
          tabNotUpdated.identifier -> viewModel.loadNotUpdated()
        }
      }
    }
    return false
  }

  private fun selectTabTitle() {
    toolbar.title = when (drawer.currentSelection) {
      tabAll.identifier -> getString(R.string.all)
      tabTimedOut.identifier -> getString(R.string.outdated_plural)
      tabNotUpdated.identifier -> getString(R.string.not_registered)
      else -> getString(R.string.history)
    }
  }

  private fun addChangeTabListener(drawer: Drawer) {
    drawer.setOnDrawerItemClickListener { _, _, drawerItem ->
      drawer.closeDrawer()
      viewModel.activityStatus.value = ActivityStatus.Loading
      selectTabList(drawerItem.identifier.toInt())
    }
  }

  private fun editFragmentCompleteEdition() {
    editFragment.fadeOutAnim()
    super.onBackPressed()
    detailsFragment.fadeIn()
    if (isTablet()) selectTabList(drawer.currentSelection.toInt())
  }

  @Suppress("PLUGIN_WARNING")
  private fun tabletCloseSecondFragment() {
    if (secondary_container.isVisible()) {
      secondary_container.gone()
      selectTabTitle()
    }
  }

  private fun currentActiveFragment() = supportFragmentManager.fragments.filterNotNull().last()

  private fun hideFABs() {
    returnButton.hide()
    giveButton.hide()
  }

  private fun showFABs() {
    returnButton.show()
    giveButton.show()
  }
}
