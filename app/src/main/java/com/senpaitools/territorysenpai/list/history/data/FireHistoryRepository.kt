package com.senpaitools.territorysenpai.list.history.data

import com.google.firebase.firestore.FirebaseFirestore
import com.senpaitools.territorysenpai.common.domain.model.CONGREGATION
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.list.history.domain.model.History
import com.senpaitools.territorysenpai.list.history.domain.repository.HistoryRepository
import nl.komponents.kovenant.deferred


class FireHistoryRepository : HistoryRepository {
  private val firebase: FirebaseFirestore = FirebaseFirestore.getInstance()

  override fun getHistory(): List<History> {
    val data = hashMapOf<String, Boolean>()
    data["updated"] = true
    val deferred = deferred<List<History>, Exception>()
    firebase.collection("congregation").document(CONGREGATION)
      .collection("history").get().addOnCompleteListener {
        if (it.isSuccessful) deferred.resolve(
          it.result?.map { it.toObject(History::class.java) } ?: listOf()
        )
        else deferred.reject(Exception("Firebase failed"))

      }
    return deferred.promise.get()
  }

  override fun addHistoryRegister(history: History): History {
    val deferred = deferred<History, Exception>()
    firebase.collection("congregation").document(CONGREGATION)
      .collection("history").get().addOnCompleteListener {
        if (it.isSuccessful) {
          firebase.collection("congregation").document(CONGREGATION)
            .collection("history").document(it.result?.count().toString())
            .set(history).addOnCompleteListener {
              if (it.isSuccessful) {
                deferred.resolve(history)
              } else {
                deferred.reject(Exception("Firebase failed"))
              }
            }
        } else {
        }
      }
    return deferred.promise.get()
  }

  override fun replaceTerritoryRegister(history: History) {
    firebase.collection("congregation").document(CONGREGATION)
      .collection("history").document(history.id.toString()).set(history)
  }

  override fun getHistoryCount(): Int {
    val deferred = deferred<Int, Exception>()
    firebase.collection("congregation").document(CONGREGATION)
      .collection("history").get().addOnCompleteListener {
        if (it.isSuccessful) {
          deferred.resolve(it.result?.size() ?: 0)
        } else {
          deferred.reject(Exception("Firebase could not read History size"))
        }
      }
    return deferred.promise.get()
  }

  override fun clearTerritoryHistory(territory: Territory) {
    val editor = firebase.batch()
    val collection =
      firebase.collection("congregation").document(CONGREGATION).collection("history")
    collection.whereEqualTo("number", territory.number).get().addOnCompleteListener { query ->
      query.result?.map { it.toObject(History::class.java).id }?.forEach {
        editor.update(collection.document(it.toString()), "updated", true)
      }
      editor.commit().addOnCompleteListener {
        if (it.isSuccessful) {

        } else {
          throw Exception("clear territory history failed")
        }
      }
    }
  }
}
