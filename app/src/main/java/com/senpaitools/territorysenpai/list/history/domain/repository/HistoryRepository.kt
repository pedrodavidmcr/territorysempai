package com.senpaitools.territorysenpai.list.history.domain.repository

import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.list.history.domain.model.History

interface HistoryRepository {
  fun getHistory(): List<History>
  fun addHistoryRegister(history: History): History
  fun replaceTerritoryRegister(history: History)
  fun getHistoryCount(): Int
  fun clearTerritoryHistory(territory: Territory)
}