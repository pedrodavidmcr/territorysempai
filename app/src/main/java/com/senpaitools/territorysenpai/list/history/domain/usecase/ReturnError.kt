package com.senpaitools.territorysenpai.list.history.domain.usecase

import com.senpaitools.territorysenpai.common.domain.usecase.NotSuccessful

sealed class ReturnError : NotSuccessful {
  object NoTerritoriesToReturn : ReturnError()
}