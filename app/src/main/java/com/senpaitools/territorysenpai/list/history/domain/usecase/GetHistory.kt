package com.senpaitools.territorysenpai.list.history.domain.usecase

import com.senpaitools.territorysenpai.common.domain.model.FORMAT
import com.senpaitools.territorysenpai.common.domain.usecase.Either
import com.senpaitools.territorysenpai.common.domain.usecase.UseCase
import com.senpaitools.territorysenpai.common.domain.usecase.either
import com.senpaitools.territorysenpai.list.history.domain.model.History
import com.senpaitools.territorysenpai.list.history.domain.repository.HistoryRepository
import com.senpaitools.territorysenpai.list.territories.usecase.GetError
import khronos.toDate

class GetHistory(private val repository: HistoryRepository) : UseCase<List<History>, GetError> {
  override fun run(): Either<GetError, List<History>> =
    either {
      val history = repository.getHistory().sortedByDescending { it.date.toDate(FORMAT) }
      return@either if (history.isEmpty()) GetError.NoHistory
      else history
    }
}