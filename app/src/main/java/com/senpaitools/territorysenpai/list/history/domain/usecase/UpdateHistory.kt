package com.senpaitools.territorysenpai.list.history.domain.usecase

import android.util.Log
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.list.history.data.FireHistoryRepository
import com.senpaitools.territorysenpai.list.history.domain.model.History

class UpdateHistory(
  val territory: Territory,
  val repository: FireHistoryRepository,
  val history: List<History>
) {
  fun execute() {
    val historyToReplace: List<History> = history.map { it.copy(updated = true) }
    Log.v("UPDATE_HISTORY", historyToReplace.toString())
    historyToReplace.forEach { repository.replaceTerritoryRegister(it) }
    val historyUpdated = repository.getHistory()
  }

}