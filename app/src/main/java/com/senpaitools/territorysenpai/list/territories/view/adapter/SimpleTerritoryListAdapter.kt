package com.senpaitools.territorysenpai.list.territories.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.senpaitools.territorysenpai.R
import com.senpaitools.territorysenpai.common.domain.model.Status
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.view.adapter.DataAdapter
import com.senpaitools.territorysenpai.common.view.extension.*
import kotlinx.android.synthetic.main.items_available.view.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.textColor


class SimpleTerritoryListAdapter(private val listener: (Territory) -> Unit) :
  DataAdapter<Territory, SimpleTerritoryListAdapter.Holder>() {

  override fun onBindViewHolder(holder: Holder, position: Int) =
    holder.setData(dataList[position])

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder =
    Holder(
      LayoutInflater.from(parent.context)
        .inflate(R.layout.items_available, parent, false)
    )


  inner class Holder(private val territoryView: View) : RecyclerView.ViewHolder(territoryView) {
    fun setData(territory: Territory) {
      territoryView.territoryNumber.text = territory.number.toString()
      setBackgroundColorByTerritoryStatus(territory, territoryView)
      highlightNumberColorIfTerritoryIsNotUpdated(territory.updated, territoryView.territoryNumber)
      territoryView.setOnClickListener { listener(territory) }
    }
  }

  private fun highlightNumberColorIfTerritoryIsNotUpdated(updated: Boolean, number: TextView?) {
    number!!.textColor = if (updated) number.context!!.colorWhite()
    else number.context!!.colorRed()
  }

  private fun setBackgroundColorByTerritoryStatus(territory: Territory, territoryView: View) {
    territoryView.backgroundColor = when {
      territory.isTimedOut() -> territoryView.context!!.colorTimedOut()
      territory.status == Status.TAKEN -> territoryView.context!!.colorTaken()
      else -> territoryView.context!!.colorPrimary()
    }
  }
}