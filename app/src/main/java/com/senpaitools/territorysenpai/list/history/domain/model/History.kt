package com.senpaitools.territorysenpai.list.history.domain.model

data class History(
  val date: String = "", val number: Int = -1,
  val owner: String = "", val action: Action = Action.RETURN,
  val updated: Boolean = true, val id: Int = -1
)