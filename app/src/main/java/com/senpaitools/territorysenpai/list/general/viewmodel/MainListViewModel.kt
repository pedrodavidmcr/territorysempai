package com.senpaitools.territorysenpai.list.general.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.executor.execute
import com.senpaitools.territorysenpai.common.inject.Injector
import com.senpaitools.territorysenpai.list.general.ActivityStatus
import com.senpaitools.territorysenpai.list.history.domain.model.History
import com.senpaitools.territorysenpai.list.territories.usecase.GetError

class MainListViewModel : ViewModel() {
  val territoryList: MutableLiveData<List<Territory>> = MutableLiveData()
  val history: MutableLiveData<List<History>> = MutableLiveData()
  val error: MutableLiveData<GetError> = MutableLiveData()
  val activityStatus: MutableLiveData<ActivityStatus> = MutableLiveData()
  val selectedTerritory: MutableLiveData<Territory> = MutableLiveData()
  val changesOfTerritory: MutableLiveData<List<History>> = MutableLiveData()

  fun loadAll() = execute(
    Injector.getGetAllTerritories(),
    territoryList, error
  )

  fun loadNotUpdated() = execute(
    Injector.getGetNotUpdatedTerritories(),
    territoryList, error
  )

  fun loadTimedOut() = execute(
    Injector.getGetTimedOutTerritories(),
    territoryList, error
  )

  fun loadHistory() = execute(
    Injector.getGetHistory(),
    history, error
  )

  fun loadChangesFromTerritory(territory: Territory) = execute(
    Injector.getGetChangesOfTerritory(territory),
    changesOfTerritory, error
  )

  fun putChangesAsRegistered(territory: Territory) = execute(
    Injector.getRegisterChangesFromTerritory(territory),
    history, error
  )

  fun editTerritory(new: Territory, old: Territory) = execute(
    Injector.getUpdateTerritory(new, old),
    selectedTerritory, error
  )
}