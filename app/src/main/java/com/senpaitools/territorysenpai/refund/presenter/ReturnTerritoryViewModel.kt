package com.senpaitools.territorysenpai.refund.presenter

import androidx.lifecycle.MutableLiveData
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.executor.execute
import com.senpaitools.territorysenpai.common.inject.Injector
import com.senpaitools.territorysenpai.common.viewmodel.BackupViewModel

class ReturnTerritoryViewModel : BackupViewModel() {
  val territoriesToReturn: MutableLiveData<List<Territory>> = MutableLiveData()

  fun load() = loadTerritoriesToReturn()

  private fun loadTerritoriesToReturn() = execute(
    Injector.getGetTakenTerritories(),
    territoriesToReturn, error
  )

  fun returnTerritory(territory: Territory) {
    execute(
      Injector.getReturnTerritory(territory),
      updatedTerritory, error
    )
    backupTerritory.value = territory
  }


}