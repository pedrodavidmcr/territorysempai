package com.senpaitools.territorysenpai.refund.view.activity

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.senpaitools.territorysenpai.R
import com.senpaitools.territorysenpai.common.view.activity.BackupActivity
import com.senpaitools.territorysenpai.refund.view.fragment.ReturnTerritoryFragment
import kotlinx.android.synthetic.main.activity_return_territory.*

class ReturnTerritoryActivity : BackupActivity() {
  private val listFragment: ReturnTerritoryFragment = ReturnTerritoryFragment()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_return_territory)
    updateFragment(listFragment)
  }

  private fun updateFragment(fragment: Fragment) =
    super.updateFragment(R.id.returnContainer, fragment)

  fun quitList() = super.quitList(returnContainer, listFragment)

  fun showDetails(bundle: Bundle) = super.showDetails(R.id.returnContainer, bundle)
}
