package com.senpaitools.territorysenpai.refund.domain.usecase

import com.senpaitools.territorysenpai.common.domain.model.Status
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.repository.TerritoryRepository
import com.senpaitools.territorysenpai.common.domain.usecase.Either
import com.senpaitools.territorysenpai.common.domain.usecase.UseCase
import com.senpaitools.territorysenpai.common.domain.usecase.either
import com.senpaitools.territorysenpai.list.history.domain.usecase.ReturnError

class GetTakenTerritories(
  private val repository: TerritoryRepository
) : UseCase<List<Territory>, ReturnError> {
  override fun run(): Either<ReturnError, List<Territory>> =
    either {
      val territoryList = repository.getAllTerritories("cabanyal")
        .filter { it.status == Status.TAKEN }
        .sortedBy { it.number }
      return@either if (territoryList.isEmpty()) ReturnError.NoTerritoriesToReturn
      else territoryList
    }
}

