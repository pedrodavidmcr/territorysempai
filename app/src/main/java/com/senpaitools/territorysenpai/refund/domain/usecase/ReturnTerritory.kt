package com.senpaitools.territorysenpai.refund.domain.usecase

import com.senpaitools.territorysenpai.common.domain.model.FORMAT
import com.senpaitools.territorysenpai.common.domain.model.Status
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.repository.TerritoryRepository
import com.senpaitools.territorysenpai.common.domain.usecase.Either
import com.senpaitools.territorysenpai.common.domain.usecase.either
import com.senpaitools.territorysenpai.list.history.domain.model.Action
import com.senpaitools.territorysenpai.list.history.domain.model.History
import com.senpaitools.territorysenpai.list.history.domain.repository.HistoryRepository
import com.senpaitools.territorysenpai.list.history.domain.usecase.ActionUseCase
import com.senpaitools.territorysenpai.list.history.domain.usecase.ReturnError
import khronos.Dates.today
import khronos.toString

class ReturnTerritory(
  private val repository: TerritoryRepository,
  history: HistoryRepository,
  private val territory: Territory
) : ActionUseCase<Territory, ReturnError>(
  history,
  History(today.toString(FORMAT), territory.number, territory.owner, Action.RETURN, false)
) {
  override fun run(): Either<ReturnError, Territory> =
    either {
      return@either repository.updateTerritory(
        territory.copy(
          status = Status.AVAILABLE,
          updated = false,
          returnDate = today.toString(FORMAT)
        ),
        territory
      )
    }
}