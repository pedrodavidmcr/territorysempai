package com.senpaitools.territorysenpai.refund.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.senpaitools.territorysenpai.R
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.services.hideKeyboard
import com.senpaitools.territorysenpai.common.view.animation.SCREEN_POSITION
import com.senpaitools.territorysenpai.common.view.animation.TYPE
import com.senpaitools.territorysenpai.common.view.animation.generateAnimationValuesOf
import com.senpaitools.territorysenpai.common.view.extension.*
import com.senpaitools.territorysenpai.list.history.domain.usecase.ReturnError
import com.senpaitools.territorysenpai.refund.presenter.ReturnTerritoryViewModel
import com.senpaitools.territorysenpai.refund.view.activity.ReturnTerritoryActivity
import com.senpaitools.territorysenpai.refund.view.adapter.SelectTerritoryDetailsAdapter
import kotlinx.android.synthetic.main.fragment_return_territory.*

class ReturnTerritoryFragment : Fragment() {
  private val adapter: SelectTerritoryDetailsAdapter by lazy {
    SelectTerritoryDetailsAdapter { hasSelectedItem ->
      if (hasSelectedItem) returnButton.show()
      else returnButton.hide()
      view?.hideKeyboard()
    }
  }
  private val viewModel: ReturnTerritoryViewModel by lazy {
    ViewModelProviders.of(activity!!).get(ReturnTerritoryViewModel::class.java)
  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.fragment_return_territory, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    viewModel.load()
    setUpSearchBar()
    setUpFAB()
    setUpTerritoryList()
    setUpToolbar()
    setUpErrorHandling()
  }

  private fun setDataIntoList(list: List<Territory>) {
    view?.run {
      loading.gone()
      returnList.visible()
      adapter.setData(list)
    }
  }

  private fun showListIsEmpty() {
    loading?.gone()
    container.addViewConstrainedToCenter(
      context!!.generateMessageTextView(getString(R.string.on_empty_return_list))
    )
  }

  private fun setUpSearchBar() {
    search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
      override fun onQueryTextSubmit(query: String?): Boolean = searchElementInAdapter(query)
      override fun onQueryTextChange(newText: String?): Boolean = searchElementInAdapter(newText)
    })
  }

  private fun searchElementInAdapter(query: String?): Boolean {
    adapter.deselectTerritories()
    adapter.updateData(adapter.initialData!!
      .filter {
        it.owner
          .toLowerCase()
          .indexOf(query!!.toLowerCase()) != -1
      })
    return true
  }

  private fun setUpFAB() {
    returnButton.setOnClickListener {
      (activity as ReturnTerritoryActivity).backupTerritory = adapter.currentSelectedTerritory!!
      adapter.currentSelectedTerritory?.let { territory ->
        viewModel.returnTerritory(territory)
        animateToConfirmationScreen(it)
      }

    }
  }

  private fun animateToConfirmationScreen(view: View) {
    val animationValue = generateAnimationValuesOf(view)
    val extras = Bundle()
    extras.putSerializable(SCREEN_POSITION, animationValue)
    extras.putString(TYPE, "RETURN")
    (activity as ReturnTerritoryActivity).showDetails(extras)
  }

  private fun setUpTerritoryList() {
    returnList.adapter = adapter
    returnList.layoutManager = LinearLayoutManager(context)
    returnList.setHideKeyboardWhenScroll()
    viewModel.territoriesToReturn.observe(this, Observer {
      setDataIntoList(it)
    })
  }

  private fun setUpErrorHandling() {
    viewModel.error.observe(this, Observer {
      when (it) {
        ReturnError.NoTerritoriesToReturn -> showListIsEmpty()
      }
    })
  }

  private fun setUpToolbar() {
    toolbar.navigationIcon = getDrawable(R.drawable.back)
    toolbar.setNavigationOnClickListener { activity!!.finish() }
  }
}