package com.senpaitools.territorysenpai.refund.view.adapter

import android.animation.ObjectAnimator
import android.app.Activity
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.senpaitools.territorysenpai.R
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.view.adapter.DataAdapter
import com.senpaitools.territorysenpai.common.view.extension.loadDrawable
import kotlinx.android.synthetic.main.items_return.view.*


class SelectTerritoryDetailsAdapter(private val listener: (Boolean) -> Unit) :
  DataAdapter<Territory, SelectTerritoryDetailsAdapter.Holder>() {

  var initialData: List<Territory>? = null
  var currentSelectedTerritory: Territory? = null
  private var selectedItem: View? = null

  override fun onBindViewHolder(holder: Holder, position: Int) =
    holder.setData(dataList[position])

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder =
    Holder(
      LayoutInflater.from(parent.context)
        .inflate(R.layout.items_return, parent, false)
    )

  override fun getItemId(position: Int): Long = position.toLong()
  override fun getItemViewType(position: Int): Int = position
  inner class Holder(private val item: View) : RecyclerView.ViewHolder(item) {
    fun setData(territory: Territory) {
      if (item.territoryNumber.text.isNotBlank())
        item.territoryNumber.text = territory.number.toString()
      item.territoryOwner.text = territory.owner
      item.territoryStatus.text = if (territory.isTimedOut())
        item.context.getString(R.string.outdated)
      else item.context.getString(R.string.not_outdated)
      item.territoryTakenDate.text = territory.takenDate

      item.setOnClickListener {
        animateSelection(it)
        selectedItem?.let {
          animateRejection(it, currentSelectedTerritory!!)
        }
        currentSelectedTerritory = territory
        selectedItem = if (it == selectedItem)
          null
        else it
        listener(selectedItem != null)
      }

    }
  }

  private fun animateSelection(view: View) {
    val animation = ObjectAnimator
      .ofFloat(view.territoryNumber, "rotationY", 180F)
    animation.duration = 400
    Handler().postDelayed({
      changeItemData(view, "", R.drawable.text_shape_active, R.color.divider)
    }, 200)
    animation.start()
  }

  private fun animateRejection(view: View, territory: Territory) {
    val animation = ObjectAnimator
      .ofFloat(view.territoryNumber, "rotationY", 0F)
    animation.duration = 400
    Handler().postDelayed({
      changeItemData(
        view, territory.number.toString(), R.drawable.text_shape,
        R.color.colorPrimaryLight
      )
    }, 200)
    animation.start()
  }

  private fun changeItemData(view: View, text: String, newShape: Int, newColor: Int) {
    view.territoryNumber.text = text
    view.territoryNumber.background = (view.context as Activity).loadDrawable(newShape)
    view.root.background = ContextCompat.getDrawable(view.context, newColor)
  }

  fun setData(data: List<Territory>) {
    initialData = data
    updateData(initialData!!)
  }

  fun deselectTerritories() = selectedItem?.let {
    animateRejection(it, currentSelectedTerritory!!)
    selectedItem = null
    currentSelectedTerritory = null
  }


}

