package com.senpaitools.territorysenpai.common.domain.usecase

interface UseCase<V, E> {
  fun run(): Either<E, V>
}