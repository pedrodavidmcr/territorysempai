package com.senpaitools.territorysenpai.common.domain.usecase

import com.senpaitools.territorysenpai.common.domain.model.FORMAT
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.repository.TerritoryRepository
import com.senpaitools.territorysenpai.list.history.data.FireHistoryRepository
import com.senpaitools.territorysenpai.list.history.domain.model.Action
import com.senpaitools.territorysenpai.list.history.domain.model.History
import com.senpaitools.territorysenpai.list.history.domain.usecase.ActionUseCase
import com.senpaitools.territorysenpai.list.territories.usecase.GetError
import khronos.Dates.today
import khronos.toString

class UpdateTerritory(
  private val repository: TerritoryRepository,
  private val newTerritory: Territory,
  private val previousTerritory: Territory
) : ActionUseCase<Territory, GetError>(
  FireHistoryRepository(), History(
    today.toString(FORMAT),
    newTerritory.number,
    newTerritory.owner,
    Action.EDIT,
    true
  )
) {
  override fun run(): Either<GetError, Territory> =
    either {
      return@either repository
        .updateTerritory(newTerritory, previousTerritory)
    }
}