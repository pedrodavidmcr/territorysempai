package com.senpaitools.territorysenpai.common.domain.usecase

import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.repository.TerritoryRepository

class UndoUpdate(
  private val repository: TerritoryRepository,
  private val newTerritory: Territory,
  private val previousTerritory: Territory
) : UseCase<Territory, NotSuccessful> {
  override fun run(): Either<NotSuccessful, Territory> =
    either {
      return@either repository.updateTerritory(newTerritory, previousTerritory)
    }
}