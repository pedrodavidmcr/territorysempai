package com.senpaitools.territorysenpai.common.view.fragment


import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.senpaitools.territorysenpai.R
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.view.activity.BackupActivity
import com.senpaitools.territorysenpai.common.view.animation.*
import com.senpaitools.territorysenpai.common.view.extension.*
import com.senpaitools.territorysenpai.common.viewmodel.BackupViewModel
import com.senpaitools.territorysenpai.give.view.activity.GiveTerritoryActivity
import com.senpaitools.territorysenpai.give.viewmodel.GiveTerritoryViewModel
import com.senpaitools.territorysenpai.refund.presenter.ReturnTerritoryViewModel
import com.senpaitools.territorysenpai.refund.view.activity.ReturnTerritoryActivity
import kotlinx.android.synthetic.main.fragment_confirmation.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.textColor

class ConfirmationFragment : Fragment() {
  private val backupActivity: BackupActivity by lazy { activity!! as BackupActivity }
  private lateinit var finishedActionText: String
  private val viewModel: BackupViewModel by lazy {
    when (arguments!!.getString(TYPE)!!) {
      "GIVE" -> ViewModelProviders.of(activity!!).get(GiveTerritoryViewModel::class.java)
      else -> ViewModelProviders.of(activity!!).get(ReturnTerritoryViewModel::class.java)
    }
  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    val view = inflater.inflate(R.layout.fragment_confirmation, container, false)
    registerCircleRevealAnimation(
      view!!,
      arguments!!.getSerializable(SCREEN_POSITION) as RevealAnimationSettings,
      context!!.colorPrimaryDark(),
      context!!.colorAccent()
    )
    return view
  }

  private fun customizeTerritoryCardAndTitles(action: String) {
    when (action) {
      "GIVE" -> customizeToGiveTerritory()
      else -> customizeToReturnTerritory()
    }
  }

  private fun customizeToReturnTerritory() {
    pendingAction.text = getString(R.string.returning_territory)
    finishedActionText = getString(R.string.territory_returned)
    returnDateTitle.text = getString(R.string.return_date)
    takenDateTitle.text = getString(R.string.taken_date)
  }

  private fun customizeToGiveTerritory() {
    pendingAction.text = getString(R.string.giving_territory)
    finishedActionText = getString(R.string.territory_delivered)
    returnDateTitle.text = getString(R.string.outdate_date)
    takenDateTitle.text = getString(R.string.taken_date_two)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    customizeTerritoryCardAndTitles(arguments!!.getString(TYPE)!!)
    initConfirmButton(view)
    initUndoButton(view)
    viewModel.updatedTerritory.observe(activity!!, Observer { territory ->
      territory?.let {
        onTerritoryIsUpdatedSuccessfully(it)
      }
    })
  }

  private fun initUndoButton(rootView: View) {
    undoButton.setOnClickListener {
      if (isPreLollipop()) {
        viewModel.undoAction()
        backupActivity.backToList()
      } else {
        undoActionAndGoBackWithAnimation(rootView)
      }
    }
  }

  @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
  private fun undoActionAndGoBackWithAnimation(rootView: View) {
    startCircleExitAnimation(
      rootView,
      arguments!!.getSerializable(SCREEN_POSITION) as RevealAnimationSettings,
      this@ConfirmationFragment.context!!.colorWhite(),
      this@ConfirmationFragment.context!!.colorAccent(),
      doBeforeFinish = {
        viewModel.undoAction()
        backupActivity.backToList()
      }
    )
  }


  private fun initConfirmButton(rootView: View) {
    confirmButton.setOnClickListener {
      if (activity is GiveTerritoryActivity) (activity as GiveTerritoryActivity).quitList()
      else if (activity is ReturnTerritoryActivity) (activity as ReturnTerritoryActivity).quitList()
      if (isPreLollipop()) {
        activity!!.finish()
      } else {
        finishActivityWithCircleAnimation(rootView)
      }
    }
  }

  @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
  private fun finishActivityWithCircleAnimation(rootView: View) {
    startCircleExitAnimation(
      rootView,
      arguments!!.getSerializable(MENU_POSITION) as RevealAnimationSettings,
      this@ConfirmationFragment.context!!.colorWhite(),
      this@ConfirmationFragment.context!!.colorAccent(),
      doBeforeFinish = activity!!::finish
    )
  }

  private fun onTerritoryIsUpdatedSuccessfully(territory: Territory) {
    view?.let {
      showTerritoryData(territory)
      animateCardViews()
    }
  }

  private fun animateCardViews() {
    ObjectAnimator.ofInt(
      infoContainer.measuredHeight,
      (infoContainer.measuredHeight * 0.20).toInt()
    ).run {
      addUpdateListener { changeSizeAndTextInfoCardView(it) }
      setOnEndListener { makeTerritoryAndButtonsVisible() }
      duration = 500
      start()
    }
  }

  private fun makeTerritoryAndButtonsVisible() {
    territoryContainer.visible()
    undoButton.visible()
    confirmButton.visible()
    territoryContainer.scaleY = 0F
    confirmButton.scaleY = 0F
    undoButton.scaleY = 0F
    territoryContainer.animate().scaleY(1F).setDuration(200).start()
    confirmButton.animate().scaleY(1F).setDuration(500).start()
    undoButton.animate().scaleY(1F).setDuration(500).start()
  }

  private fun changeSizeAndTextInfoCardView(animator: ValueAnimator) {
    val params = infoContainer.layoutParams
    params.height = animator.animatedValue as Int
    infoContainer.layoutParams = params
    infoContainer.backgroundColor = context!!.colorAccent()
    pendingAction.text = finishedActionText
    pendingAction.textColor = context!!.colorWhite()
    infoContainer.removeView(loading)
  }

  private fun showTerritoryData(territory: Territory) = territory.run {
    zoneText.text = zone
    ownerText.text = owner
    numberText.text = number.toString()
    takenDateText.text = takenDate
    returnDateText.text = returnDate
  }


}

