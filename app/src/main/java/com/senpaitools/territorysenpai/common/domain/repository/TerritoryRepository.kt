package com.senpaitools.territorysenpai.common.domain.repository

import com.senpaitools.territorysenpai.common.domain.model.Territory

interface TerritoryRepository {
  fun getAllTerritories(congregation: String): List<Territory>
  fun updateTerritory(newTerritory: Territory, previousTerritory: Territory): Territory
}