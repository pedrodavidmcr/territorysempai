package com.senpaitools.territorysenpai.common.domain.usecase

sealed class Either<out E, out V> {
  data class Error<out E>(val error: E) : Either<E, Nothing>()
  data class Value<out V>(val value: V) : Either<Nothing, V>()
}

fun <V> value(value: V): Either<Nothing, V> =
  Either.Value(value)

fun <E> error(value: E): Either<E, Nothing> =
  Either.Error(value)

inline fun <reified V, reified E> either(action: () -> Any): Either<E, V> {
  val result = action()
  return when (result) {
    is E -> error(result)
    is V -> value(result)
    else -> TODO()
  }
}

inline fun <E, V> Either<E, V>.fold(notSuccessful: (E) -> Unit, success: (V) -> Unit) =
  when (this) {
    is Either.Error -> notSuccessful(this.error)
    is Either.Value -> success(this.value)
  }