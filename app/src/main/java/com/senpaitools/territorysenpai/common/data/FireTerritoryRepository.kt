package com.senpaitools.territorysenpai.common.data

import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore
import com.senpaitools.territorysenpai.common.domain.model.CONGREGATION
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.repository.TerritoryRepository
import com.senpaitools.territorysenpai.common.executor.runOnIoThread
import nl.komponents.kovenant.deferred

class FireTerritoryRepository : TerritoryRepository {
  private val firebase: FirebaseFirestore = FirebaseFirestore.getInstance()

  override fun getAllTerritories(congregation: String): List<Territory> {
    val deferred = deferred<List<Territory>, Exception>()
    runOnIoThread {
      firebase.collection("congregation").document(CONGREGATION)
        .collection("territory").get().addOnCompleteListener {
          if (it.isSuccessful) {
            deferred.resolve(it.result?.map { it.toObject(Territory::class.java) } ?: listOf())
          } else {
            deferred.reject(Exception("Firebase failed"))
          }
        }
    }
    return deferred.promise.get()
  }

  override fun updateTerritory(newTerritory: Territory, previousTerritory: Territory): Territory =
    if (newTerritory.number == previousTerritory.number) updateTerritory(newTerritory)
    else replaceTerritory(previousTerritory.number, newTerritory)

  private fun replaceTerritory(number: Int, newTerritory: Territory): Territory {
    val deferred = deferred<Territory, Exception>()
    runOnIoThread {
      val collection =
        firebase.collection("congregation").document(CONGREGATION).collection("territory")
      collection.document(newTerritory.number.toString()).get().addOnCompleteListener {
        if (it.isSuccessful && it.result?.exists() == false) {
          collection.document(newTerritory.number.toString()).set(newTerritory)
          collection.document(number.toString()).delete()
          deferred.resolve(newTerritory)
        } else deferred.reject(Exception("Numero ya en uso"))
      }
    }
    return deferred.promise.get()
  }

  private fun updateTerritory(newTerritory: Territory): Territory {
    Log.d("STATE IS", newTerritory.toString())
    val deferred = deferred<Territory, Exception>()
    runOnIoThread {
      firebase.collection("congregation").document(CONGREGATION).collection("territory")
        .document(newTerritory.number.toString()).set(newTerritory).addOnCompleteListener {
          if (it.isSuccessful) deferred.resolve(newTerritory)
          else deferred.reject(Exception())
        }
    }
    return deferred.promise.get()
  }

}