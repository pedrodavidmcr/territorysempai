package com.senpaitools.territorysenpai.common.view.animation

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.os.Build
import android.os.Handler
import android.view.View
import android.view.ViewAnimationUtils
import androidx.annotation.RequiresApi
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import org.jetbrains.anko.backgroundColor

const val MENU_POSITION = "menu"
const val SCREEN_POSITION = "screen"
const val TYPE = "type"

fun registerCircleRevealAnimation(
  view: View, settings: RevealAnimationSettings,
  startColor: Int, endColor: Int
) {
  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
    view.addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
      @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
      override fun onLayoutChange(
        view: View?,
        p1: Int,
        p2: Int,
        p3: Int,
        p4: Int,
        p5: Int,
        p6: Int,
        p7: Int,
        p8: Int
      ) {
        view!!.removeOnLayoutChangeListener(this)
        val radius = Math.sqrt(
          (settings.width * settings.width + settings.height * settings.height)
            .toDouble()
        ).toFloat()
        val anim = ViewAnimationUtils.createCircularReveal(
          view, settings.centerX, settings.centerY,
          0F, radius
        )
        anim.duration = 500
        anim.interpolator = FastOutSlowInInterpolator()
        anim.start()
        startColorAnimation(view, startColor, endColor, 150)
      }
    })
  }
}


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun startCircleExitAnimation(
  view: View, settings: RevealAnimationSettings, startColor: Int,
  endColor: Int, doBeforeFinish: () -> Unit, finalRadious: Float = 0F
) {

  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

    val radius = Math.sqrt(
      (settings.width * settings.width + settings.height * settings.height)
        .toDouble()
    ).toFloat()
    val anim = ViewAnimationUtils.createCircularReveal(
      view, settings.centerX, settings.centerY,
      radius, finalRadious
    )
    anim.duration = 700
    anim.interpolator = FastOutSlowInInterpolator()
    Handler().postDelayed(doBeforeFinish, 690)
    anim.start()
//    view.backgroundColor = endColor
    startColorAnimation(view, startColor, endColor, 350)
  }
}

fun startColorAnimation(
  view: View?,
  startColor: Int,
  endColor: Int,
  duration: Int,
  whenFinished: () -> Unit = {}
) {
  ValueAnimator().apply {
    setIntValues(startColor, endColor)
    setEvaluator(ArgbEvaluator())
    addUpdateListener {
      view!!.backgroundColor = it.animatedValue as Int
    }
    this.duration = duration.toLong()
    setOnEndListener { whenFinished() }
  }.start()


}

fun coordinatesForCenterOf(view: View): Pair<Int, Int> =
  Pair(view.x.toInt() + view.width / 2, view.y.toInt() + view.height / 2)


fun generateAnimationValuesOf(view: View): RevealAnimationSettings {
  val (centerX, centerY) = coordinatesForCenterOf(view)
  return RevealAnimationSettings(centerX, centerY, view.rootView.width, view.rootView.height)
}