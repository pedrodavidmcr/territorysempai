package com.senpaitools.territorysenpai.common.view.animation

import java.io.Serializable

data class RevealAnimationSettings(
  val centerX: Int,
  val centerY: Int,
  val width: Int,
  val height: Int
) : Serializable