package com.senpaitools.territorysenpai.common.view.extension

import androidx.recyclerview.widget.RecyclerView
import com.senpaitools.territorysenpai.common.services.hideKeyboard

fun RecyclerView.setHideKeyboardWhenScroll() {
  this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
      if (!hasFocus()) {
        requestFocus()
        hideKeyboard()
      }
    }
  })
}
