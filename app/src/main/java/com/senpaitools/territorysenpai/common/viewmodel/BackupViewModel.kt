package com.senpaitools.territorysenpai.common.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.usecase.NotSuccessful
import com.senpaitools.territorysenpai.common.executor.execute
import com.senpaitools.territorysenpai.common.inject.Injector

open class BackupViewModel : ViewModel() {
  val error: MutableLiveData<NotSuccessful> = MutableLiveData()
  protected val backupTerritory: MutableLiveData<Territory> = MutableLiveData()
  val updatedTerritory: MutableLiveData<Territory> = MutableLiveData()

  fun undoAction() {
    execute(
      Injector.getUndoUpdate(
        backupTerritory.value!!,
        updatedTerritory.value!!
      ),
      backupTerritory, error
    )
    updatedTerritory.value = null
  }
}