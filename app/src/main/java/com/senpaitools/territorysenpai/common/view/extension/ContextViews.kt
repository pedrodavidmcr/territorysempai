package com.senpaitools.territorysenpai.common.view.extension

import android.content.Context
import android.widget.TextView
import com.senpaitools.territorysenpai.R

fun Context.generateMessageTextView(message: String): TextView = TextView(this).apply {
  text = message
  id = R.id.emptyTextView
}

