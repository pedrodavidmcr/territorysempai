package com.senpaitools.territorysenpai.common.view.extension

import android.content.Context
import androidx.core.content.ContextCompat
import com.senpaitools.territorysenpai.R

fun Context.colorWhite() = ContextCompat.getColor(this, R.color.md_white_1000)
fun Context.colorRed() = ContextCompat.getColor(this, R.color.md_red_500)
fun Context.colorPrimary() = ContextCompat.getColor(this, R.color.colorPrimary)
fun Context.colorPrimary70() = ContextCompat.getColor(this, R.color.colorPrimary70)
fun Context.colorPrimaryDark() = ContextCompat.getColor(this, R.color.colorPrimaryDark)
fun Context.colorAccent() = ContextCompat.getColor(this, R.color.colorAccent)
fun Context.colorTimedOut() = ContextCompat.getColor(this, R.color.outdated)
fun Context.colorTaken() = ContextCompat.getColor(this, R.color.taken)
fun Context.colorTransparent() = ContextCompat.getColor(this, android.R.color.transparent)

