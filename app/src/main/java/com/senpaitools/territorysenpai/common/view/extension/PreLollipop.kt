package com.senpaitools.territorysenpai.common.view.extension

import android.app.Activity
import android.os.Build
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat

fun Activity.loadDrawable(drawableRes: Int) = if (isPreLollipop())
  VectorDrawableCompat.create(resources, drawableRes, theme)
else
  ContextCompat.getDrawable(applicationContext, drawableRes)

fun Fragment.getDrawable(drawableRes: Int) = if (isPreLollipop())
  VectorDrawableCompat.create(resources, drawableRes, activity!!.theme)
else
  ContextCompat.getDrawable(context!!, drawableRes)

fun isPreLollipop() = Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP