package com.senpaitools.territorysenpai.common.view.animation

import android.animation.Animator
import android.view.animation.Animation

fun Animator.setOnEndListener(listener: () -> Unit) {
  this.addListener(object : Animator.AnimatorListener {
    override fun onAnimationRepeat(animation: Animator?) = Unit
    override fun onAnimationEnd(animation: Animator?) = listener()
    override fun onAnimationCancel(animation: Animator?) = Unit
    override fun onAnimationStart(animation: Animator?) = Unit
  })
}

fun Animation.setOnEndListener(listener: () -> Unit) {
  this.setAnimationListener(object : Animation.AnimationListener {
    override fun onAnimationRepeat(animation: Animation?) = Unit
    override fun onAnimationEnd(animation: Animation?) = listener()
    override fun onAnimationStart(animation: Animation?) = Unit
  })
}