package com.senpaitools.territorysenpai.common.view.extension

import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.constraintlayout.widget.ConstraintSet.*
import com.senpaitools.territorysenpai.R

fun ConstraintLayout.addViewConstrainedToCenter(view: View) {
  if (this.getViewById(R.id.emptyTextView) != null) this.removeEmptyListTextView()
  this.addView(view)
  val constraints = ConstraintSet()
  constraints.clone(this)
  sequenceOf(BOTTOM, TOP, LEFT, RIGHT).forEach { side ->
    constraints.connect(view.id, side, PARENT_ID, side)
  }
  constraints.applyTo(this)

}

fun ConstraintLayout.removeEmptyListTextView() {
  this.removeView(this.getViewById(R.id.emptyTextView))
}