package com.senpaitools.territorysenpai.common.view.custom

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout

class Square(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
  LinearLayout(context, attrs, defStyleAttr) {
  constructor(context: Context) : this(context, null)
  constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

  override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
    super.onMeasure(widthMeasureSpec, widthMeasureSpec)
  }
}