package com.senpaitools.territorysenpai.common.view.adapter

import androidx.recyclerview.widget.RecyclerView

abstract class DataAdapter<E, T : RecyclerView.ViewHolder?> : RecyclerView.Adapter<T>() {
  protected var dataList: List<E> = ArrayList()

  fun updateData(data: List<E>) {
    dataList = data
    notifyDataSetChanged()
  }

  fun clear() = updateData(emptyList())

  override fun getItemCount(): Int = dataList.size
}