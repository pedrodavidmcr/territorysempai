package com.senpaitools.territorysenpai.common.inject

import com.senpaitools.territorysenpai.common.data.FireTerritoryRepository
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.repository.TerritoryRepository
import com.senpaitools.territorysenpai.common.domain.usecase.UndoUpdate
import com.senpaitools.territorysenpai.common.domain.usecase.UpdateTerritory
import com.senpaitools.territorysenpai.give.domain.usecase.GetOldestTerritories
import com.senpaitools.territorysenpai.give.domain.usecase.GiveTerritory
import com.senpaitools.territorysenpai.list.history.data.FireHistoryRepository
import com.senpaitools.territorysenpai.list.history.domain.repository.HistoryRepository
import com.senpaitools.territorysenpai.list.history.domain.usecase.GetChangesOfTerritory
import com.senpaitools.territorysenpai.list.history.domain.usecase.GetHistory
import com.senpaitools.territorysenpai.list.history.domain.usecase.RegisterChangesFromTerritory
import com.senpaitools.territorysenpai.list.territories.usecase.GetAllTerritories
import com.senpaitools.territorysenpai.list.territories.usecase.GetNotUpdatedTerritories
import com.senpaitools.territorysenpai.list.territories.usecase.GetTimedOutTerritories
import com.senpaitools.territorysenpai.refund.domain.usecase.GetTakenTerritories
import com.senpaitools.territorysenpai.refund.domain.usecase.ReturnTerritory

object Injector {
  private val territoryRepository: TerritoryRepository by lazy {
    FireTerritoryRepository()
  }
  private val historyRepository: HistoryRepository by lazy {
    FireHistoryRepository()
  }

  fun getGetAllTerritories(): GetAllTerritories = GetAllTerritories(
    territoryRepository, ""
  )

  fun getGetChangesOfTerritory(territory: Territory): GetChangesOfTerritory = GetChangesOfTerritory(
    historyRepository, territory
  )

  fun getGetHistory(): GetHistory = GetHistory(historyRepository)

  fun getGetNotUpdatedTerritories(): GetNotUpdatedTerritories = GetNotUpdatedTerritories(
    territoryRepository
  )

  fun getGetOldestTerritories(): GetOldestTerritories = GetOldestTerritories(
    territoryRepository
  )

  fun getGetTakenTerritories(): GetTakenTerritories = GetTakenTerritories(
    territoryRepository
  )

  fun getGetTimedOutTerritories(): GetTimedOutTerritories = GetTimedOutTerritories(
    territoryRepository
  )

  fun getRegisterChangesFromTerritory(territory: Territory): RegisterChangesFromTerritory =
    RegisterChangesFromTerritory(
      historyRepository,
      territoryRepository, territory
    )

  fun getUndoUpdate(new: Territory, old: Territory): UndoUpdate = UndoUpdate(
    territoryRepository, new, old
  )

  fun getGiveTerritory(territory: Territory, newOwner: String): GiveTerritory = GiveTerritory(
    territoryRepository,
    historyRepository,
    territory, newOwner
  )

  fun getReturnTerritory(territory: Territory): ReturnTerritory = ReturnTerritory(
    territoryRepository,
    historyRepository, territory
  )

  fun getUpdateTerritory(new: Territory, old: Territory): UpdateTerritory =
    UpdateTerritory(territoryRepository, new, old)

}