package com.senpaitools.territorysenpai.common.domain.model

enum class Status {
  AVAILABLE, TAKEN
}