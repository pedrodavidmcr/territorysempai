package com.senpaitools.territorysenpai.common.services

import android.content.Context
import android.net.ConnectivityManager
import android.view.View
import android.view.inputmethod.InputMethodManager


fun Context.checkInternet(): Boolean {
  val connection = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
  return connection?.activeNetworkInfo?.isConnected ?: false
}

fun View.hideKeyboard() = (context.getSystemService(Context.INPUT_METHOD_SERVICE)
    as InputMethodManager)
  .hideSoftInputFromWindow(
    windowToken,
    InputMethodManager.HIDE_NOT_ALWAYS
  )
