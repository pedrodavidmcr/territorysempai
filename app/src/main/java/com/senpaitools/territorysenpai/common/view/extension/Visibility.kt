package com.senpaitools.territorysenpai.common.view.extension

import android.view.View

fun View?.isVisible() = this?.visibility == View.VISIBLE

fun View?.gone() {
  this?.visibility = View.GONE
}

fun View?.visible() {
  this?.visibility = View.VISIBLE
}


