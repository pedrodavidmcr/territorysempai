package com.senpaitools.territorysenpai.common.domain.model

import khronos.Dates.today
import khronos.months
import khronos.plus
import khronos.toDate

data class Territory(
  val number: Int = -1, val zone: String = "No Zone",
  val status: Status = Status.AVAILABLE, val takenDate: String = "",
  val returnDate: String = "", val owner: String = "No owner",
  val updated: Boolean = true
) {
  fun isTimedOut(): Boolean = (takenDate.toDate(FORMAT) + 4.months).before(today)
      && status == Status.TAKEN

  fun monthsWithoutAssignation(): Int = if (status == Status.TAKEN) 0
  else (((today.time - returnDate.toDate(FORMAT).time) / 1000) / (60 * 60 * 24 * 30)).toInt()
}

