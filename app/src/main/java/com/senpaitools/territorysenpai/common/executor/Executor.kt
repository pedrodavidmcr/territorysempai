package com.senpaitools.territorysenpai.common.executor

import androidx.lifecycle.MutableLiveData
import com.senpaitools.territorysenpai.common.domain.usecase.Either
import com.senpaitools.territorysenpai.common.domain.usecase.UseCase
import com.senpaitools.territorysenpai.common.domain.usecase.fold
import org.jetbrains.anko.doAsync
import java.util.concurrent.Executors

fun <V, E> execute(
  useCase: UseCase<V, E>,
  success: MutableLiveData<V>,
  error: MutableLiveData<in E>
) {
  useCase.doAsync {
    val result: Either<E, V> = useCase.run()
    result.fold(
      success = {
        success.postValue(it)
        error.postValue(null)
      },
      notSuccessful = {
        success.postValue(null)
        error.postValue(it)
      }
    )
  }
}

private val IO_EXECUTOR = Executors.newFixedThreadPool(4)

fun runOnIoThread(f: () -> Unit) {
  IO_EXECUTOR.execute(f)
}
