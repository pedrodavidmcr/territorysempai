package com.senpaitools.territorysenpai.common.view.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.view.animation.MENU_POSITION
import com.senpaitools.territorysenpai.common.view.animation.RevealAnimationSettings
import com.senpaitools.territorysenpai.common.view.extension.colorTransparent
import com.senpaitools.territorysenpai.common.view.fragment.ConfirmationFragment
import org.jetbrains.anko.backgroundColor

abstract class BackupActivity : AppCompatActivity() {
  protected val confirmationFragment: ConfirmationFragment = ConfirmationFragment()
  lateinit var backupTerritory: Territory

  protected fun showDetails(container: Int, bundle: Bundle) {
    bundle.putSerializable(
      MENU_POSITION, intent.getSerializableExtra(MENU_POSITION)
          as RevealAnimationSettings
    )
    confirmationFragment.arguments = bundle
    updateFragment(container, confirmationFragment)
  }

  fun backToList() {
    supportFragmentManager.beginTransaction().remove(confirmationFragment).commitAllowingStateLoss()
  }

  protected fun quitList(container: View, listFragment: Fragment) {
    container.backgroundColor = baseContext.colorTransparent()
    supportFragmentManager.beginTransaction().remove(listFragment).commitAllowingStateLoss()
  }

  protected fun updateFragment(container: Int, fragment: Fragment) {
    val transaction = supportFragmentManager.beginTransaction()
    transaction.add(container, fragment)
    transaction.commitAllowingStateLoss()

  }
}