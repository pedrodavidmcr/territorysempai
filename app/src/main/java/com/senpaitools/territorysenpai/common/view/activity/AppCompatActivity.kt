package com.senpaitools.territorysenpai.common.view.activity

import android.content.Context
import androidx.recyclerview.widget.GridLayoutManager
import com.senpaitools.territorysenpai.R

fun Context.isTablet() = resources.getBoolean(R.bool.tablet)
fun Context.getGridLayoutManagerByDeviceScreen(): GridLayoutManager =
  if (isTablet()) GridLayoutManager(this, 6)
  else GridLayoutManager(this, 4)