package com.senpaitools.territorysenpai.common.data

import com.senpaitools.territorysenpai.common.domain.model.Status.AVAILABLE
import com.senpaitools.territorysenpai.common.domain.model.Status.TAKEN
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.repository.TerritoryRepository

class MockTerritoryRepository : TerritoryRepository {
  override fun updateTerritory(newTerritory: Territory, previousTerritory: Territory): Territory {
    cabanyal[cabanyal.indexOf(previousTerritory)] = newTerritory
    return newTerritory
  }

  private val empty = emptyList<Territory>()
  val cabanyal = mutableListOf(
      Territory(1, "Cabanyal", AVAILABLE, "22/03/2016",
          "22/06/2016", "Pedro David"),
      Territory(2, "Cabanyal", AVAILABLE, "22/01/2017",
          "22/07/2016", "Pedro David"),
      Territory(3, "Castellar", AVAILABLE, "22/04/2016",
          "22/08/2016", "Marcos Marcos"),
      Territory(4, "Castellar", TAKEN, "22/09/2016",
          "22/05/2016", "Marcos Marcos"),
      Territory(50, "Castellar", AVAILABLE, "22/05/2016",
          "22/03/2016", "Marcos Marcos"),
      Territory(20, "Castellar", AVAILABLE, "22/06/2016",
          "22/01/2017", "Marcos Marcos"),
      Territory(10, "Castellar", AVAILABLE, "22/02/2016",
          "22/12/2016", "Hayley"),
      Territory(5, "Castellar", AVAILABLE, "22/01/2017",
          "12/09/2016", "Marcos Marcos"),
      Territory(78, "Castellar", AVAILABLE, "22/02/2017",
          "22/11/2016", "Marcos Marcos"),
      Territory(6, "Forn D'Alcedo", TAKEN, "22/10/2013",
          "22/02/2016", "Kela"),
      Territory(14, "Forn D'Alcedo", TAKEN, "22/12/2014",
          "21/04/2016", "Kela")
  )

  override fun getAllTerritories(congregation: String): List<Territory> {
    if (congregation == "cabanyal") return cabanyal
    return empty
  }
}