package com.senpaitools.territorysenpai.common.domain.model

import khronos.*
import khronos.Dates.today
import org.junit.Assert.assertTrue
import org.junit.Test

class TerritoryShould {
  @Test
  fun `Classify as timed out a territory if its TAKEN and 4 months have passed since takenDate`() {
    val timedOut = Territory(
      takenDate = (today - 4.months).toString("dd/MM/yyyy"),
      status = Status.TAKEN
    )
    val available = Territory(takenDate = (today - 5.months).toString("dd/MM/yyyy"))
    val correct = Territory(
      takenDate = (today + 1.day - 4.months).toString("dd/MM/yyyy"),
      status = Status.TAKEN
    )
    assertTrue(timedOut.isTimedOut())
    assertTrue(!available.isTimedOut())
    assertTrue(!correct.isTimedOut())
  }

  @Test
  fun `Get number of months without owner when calling monthsWithoutAssignation`() {
    val sixMonthsWithoutAssignation =
      Territory(returnDate = (today - 6.months).toString(FORMAT), status = Status.AVAILABLE)
    val twoMonthsWithoutAssignation =
      Territory(returnDate = (today - 2.months).toString(FORMAT), status = Status.AVAILABLE)
    val twelveMonthsWithoutAssignation =
      Territory(returnDate = (today - 12.months).toString(FORMAT), status = Status.AVAILABLE)

    assertTrue(
      5 <= sixMonthsWithoutAssignation.monthsWithoutAssignation() ||
          sixMonthsWithoutAssignation.monthsWithoutAssignation() <= 7
    )
    assertTrue(
      1 <= twoMonthsWithoutAssignation.monthsWithoutAssignation() ||
          twoMonthsWithoutAssignation.monthsWithoutAssignation() <= 3
    )
    assertTrue(
      11 <= twelveMonthsWithoutAssignation.monthsWithoutAssignation() ||
          twelveMonthsWithoutAssignation.monthsWithoutAssignation() <= 13
    )
  }
}