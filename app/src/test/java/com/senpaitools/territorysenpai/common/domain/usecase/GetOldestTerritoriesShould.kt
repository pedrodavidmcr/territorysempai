package com.senpaitools.territorysenpai.common.domain.usecase

import com.senpaitools.territorysenpai.common.data.MockTerritoryRepository
import com.senpaitools.territorysenpai.common.domain.model.FORMAT
import com.senpaitools.territorysenpai.common.domain.model.Status
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.give.domain.usecase.GetOldestTerritories
import khronos.toDate
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GetOldestTerritoriesShould {
  private val repository = MockTerritoryRepository()
  private lateinit var getOldest: GetOldestTerritories

  @Before
  fun init() {
    getOldest = GetOldestTerritories(repository)
  }

  @Test
  fun `Return a list with all available territories`() {
    val territories = getOldest.run()
    territories.fold(notSuccessful = {
      AssertionError("${it.javaClass}")
    }, success = {
      Assert.assertTrue(it.all { territory -> territory.status == Status.AVAILABLE })
    })
  }

  @Test
  fun `Give the list ordered by oldest returnDate`() {
    val territories = getOldest.run()
    territories.fold(notSuccessful = {
      AssertionError("${it.javaClass}")
    }, success = {
      Assert.assertTrue(isSortedByReturnDate(it))
    })
  }

  private fun isSortedByReturnDate(list: List<Territory>)
      : Boolean = list.map { it.returnDate.toDate(FORMAT) } ==
      list.map { it.returnDate.toDate(FORMAT) }.sorted()
}