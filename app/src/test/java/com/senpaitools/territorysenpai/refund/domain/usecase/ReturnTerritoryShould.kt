package com.senpaitools.territorysenpai.refund.domain.usecase

import com.nhaarman.mockito_kotlin.mock
import com.senpaitools.territorysenpai.common.data.MockTerritoryRepository
import com.senpaitools.territorysenpai.common.domain.model.FORMAT
import com.senpaitools.territorysenpai.common.domain.model.Status
import com.senpaitools.territorysenpai.common.domain.usecase.fold
import com.senpaitools.territorysenpai.list.history.domain.repository.HistoryRepository
import khronos.Dates.today
import khronos.toString
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class ReturnTerritoryShould {
  private val repository = MockTerritoryRepository()
  private lateinit var returnTerritory: ReturnTerritory
  private val history: HistoryRepository = mock()

  @Before
  fun setUp() {
    returnTerritory = ReturnTerritory(repository, history, repository.cabanyal[3])
  }

  @Test
  fun `Change state of territory to AVAILABLE`() {
    returnTerritory.run().fold(notSuccessful = {
      AssertionError(it.javaClass)
    }, success = {
      Assert.assertEquals(Status.AVAILABLE, it.status)
    })
  }

  @Test
  fun `Change returned date to today`() {
    returnTerritory.run().fold(notSuccessful = {
      AssertionError(it.javaClass)
    }, success = {
      Assert.assertEquals(today.toString(FORMAT), it.returnDate)
    })
  }

}