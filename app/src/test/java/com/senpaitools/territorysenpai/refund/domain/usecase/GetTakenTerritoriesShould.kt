package com.senpaitools.territorysenpai.refund.domain.usecase

import com.senpaitools.territorysenpai.common.data.MockTerritoryRepository
import com.senpaitools.territorysenpai.common.domain.model.Status
import com.senpaitools.territorysenpai.common.domain.usecase.fold
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GetTakenTerritoriesShould {
  private val repository = MockTerritoryRepository()
  private lateinit var getTaken: GetTakenTerritories

  @Before
  fun init() {
    getTaken = GetTakenTerritories(repository)
  }

  @Test
  fun `Return a list with all taken territories`() {
    getTaken.run().fold(notSuccessful = {
      AssertionError(it.javaClass)
    }, success = {
      Assert.assertTrue(it.all { territory -> territory.status == Status.TAKEN })
    })
  }

  @Test
  fun `Return the list sorted ordinarily`() {
    getTaken.run().fold(notSuccessful = {
      AssertionError(it.javaClass)
    }, success = { list ->
      Assert.assertEquals(list.sortedBy { territory -> territory.number }, list)
    })
  }
}