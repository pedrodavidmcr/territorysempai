package com.senpaitools.territorysenpai.list.territories.usecase

import com.senpaitools.territorysenpai.common.data.MockTerritoryRepository
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.usecase.fold
import org.junit.Assert
import org.junit.Assert.assertTrue
import org.junit.Test

class GetAllTerritoriesShould {
  private val repository = MockTerritoryRepository()
  @Test
  fun `Return a list of territories if there's some territories in the list`() {
    val getAllTerritories = GetAllTerritories(repository, "cabanyal")
    getAllTerritories.run().fold(
      notSuccessful = {
        AssertionError(it.javaClass)
      }, success = {
        Assert.assertFalse(it.isEmpty())
      }
    )
  }

  @Test
  fun `Return a error if there's no territories in the list`() {
    GetAllTerritories(repository, "").run().fold(
      notSuccessful = {
        Assert.assertEquals(GetError.NoTerritories, it)
      }, success = {
        AssertionError(it)
      }
    )
  }

  @Test
  fun `Return territories ordered by number`() {
    GetAllTerritories(repository, "cabanyal").run().fold(
      notSuccessful = {
        AssertionError(it.javaClass)
      }, success = {
        assertTrue(isSortedByNumber(it))
      }
    )
  }

  private fun isSortedByNumber(list: List<Territory>): Boolean = list == list.sortedBy { it.number }
}