package com.senpaitools.territorysenpai.list.territories.usecase

import com.senpaitools.territorysenpai.common.data.MockTerritoryRepository
import com.senpaitools.territorysenpai.common.domain.model.FORMAT
import com.senpaitools.territorysenpai.common.domain.model.Territory
import com.senpaitools.territorysenpai.common.domain.usecase.fold
import khronos.toDate
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class GetTimedOutTerritoriesShould {
  private val repository = MockTerritoryRepository()
  private lateinit var getTimedOut: GetTimedOutTerritories

  @Before
  fun init() {
    getTimedOut = GetTimedOutTerritories(repository)
  }

  @Test
  fun `Give a list with all timed out territories`() {
    getTimedOut.run().fold(
      notSuccessful = {
        AssertionError(it.javaClass)
      }, success = {
        assertTrue(it.all(Territory::isTimedOut))
      }
    )
  }

  @Test
  fun `Give the list ordered by oldest takenDate`() {
    getTimedOut.run().fold(
      notSuccessful = {
        AssertionError(it.javaClass)
      }, success = {
        assertTrue(isSortedByTakenDate(it))
      })
  }

  private fun isSortedByTakenDate(list: List<Territory>): Boolean =
    list.map { it.takenDate.toDate(FORMAT) } ==
        list.map { it.takenDate.toDate(FORMAT) }.sorted()
}