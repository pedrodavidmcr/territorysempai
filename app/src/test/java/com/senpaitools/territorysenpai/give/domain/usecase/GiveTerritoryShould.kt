package com.senpaitools.territorysenpai.give.domain.usecase

import com.nhaarman.mockito_kotlin.mock
import com.senpaitools.territorysenpai.common.data.MockTerritoryRepository
import com.senpaitools.territorysenpai.common.domain.model.FORMAT
import com.senpaitools.territorysenpai.common.domain.model.Status
import com.senpaitools.territorysenpai.common.domain.usecase.fold
import com.senpaitools.territorysenpai.list.history.domain.repository.HistoryRepository
import khronos.Dates.today
import khronos.months
import khronos.plus
import khronos.toString
import org.junit.Assert.assertEquals
import org.junit.Test

class GiveTerritoryShould {
  private val repository = MockTerritoryRepository()
  private val history: HistoryRepository = mock()

  @Test
  fun `Change state of territory to TAKEN`() {
    val giveTerritory = GiveTerritory(repository, history, repository.cabanyal[0], "Hayley")
    val territory = giveTerritory.run()
    territory.fold(notSuccessful = {
      AssertionError(it.javaClass)
    }, success = {
      assertEquals(Status.TAKEN, it.status)
    })

  }

  @Test
  fun `Change takenDate of territory to today`() {
    val giveTerritory = GiveTerritory(repository, history, repository.cabanyal[0], "Hayley")
    val territory = giveTerritory.run()
    territory.fold(notSuccessful = {
      AssertionError(it.javaClass)
    }, success = {
      assertEquals(today.toString(FORMAT), it.takenDate)
    })
  }

  @Test
  fun `Change old owner of territory to new owner`() {
    val newOwner = "New owner"
    val giveTerritory = GiveTerritory(repository, history, repository.cabanyal[0], newOwner)
    val territory = giveTerritory.run()
    territory.fold(notSuccessful = {
      AssertionError(it.javaClass)
    }, success = {
      assertEquals(newOwner, it.owner)
    })
  }

  @Test
  fun `Set returnDate four months after today`() {
    val giveTerritory = GiveTerritory(repository, history, repository.cabanyal[0], "Hayley")
    val territory = giveTerritory.run()
    territory.fold(notSuccessful = {
      AssertionError(it.javaClass)
    }, success = {
      assertEquals((today + 4.months).toString(FORMAT), it.returnDate)
    })
  }

}